//
//  BaseViewController.swift
//  KiddiesAhead
//
//  Created by sts on 4/6/18.
//  Copyright © 2018 mingah. All rights reserved.
//

import UIKit
import Toast_Swift
import SwiftyUserDefaults
import MBProgressHUD
import Foundation
import FAPanels

class BaseViewController: UIViewController {

    let kColorOrange = UIColor(hex: "FDB913")
  
    override func viewDidLoad() {
        super.viewDidLoad()

        hideKeyboardWhenTappedAround()
        // to enable swiping left when back button in navigation bar customized
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func isValidEmail(email:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
        
    }    

    func showAlertDialog(title: String!, message: String!, positive: String?, negative: String?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if (positive != nil) {
            
            alert.addAction(UIAlertAction(title: positive, style: .default, handler: nil))
        }
        
        if (negative != nil) {
            
            alert.addAction(UIAlertAction(title: negative, style: .default, handler: nil))
        }
        
        DispatchQueue.main.async(execute:  {
            self.present(alert, animated: true, completion: nil)
        })
    }

    func showError(_ message: String!) {

        showAlertDialog(title: "", message: message, positive:"", negative: nil)
    }

    func showSuccess(_ message: String!) {
        showAlertDialog(title: "", message: message, positive: "", negative: nil)
    }
    
    internal func showAlert(title: String?, message: String?, positive: String, negative: String?, okClosure: (() -> Void)?) {
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        let yesAction = UIAlertAction(title: positive, style: .default, handler: { (action: UIAlertAction) in
            
            if okClosure != nil {
                okClosure!()
            }
        })
        alertController.addAction(yesAction)
        if negative != nil {
            let noAction = UIAlertAction(title: negative, style: .cancel, handler: { (action: UIAlertAction) in
                
            })
            alertController.addAction(noAction)
        }
        
        self.present(alertController, animated: true, completion: nil)
        
    }
   
    
    func showToast(_ message : String) {
        self.view.makeToast(message)
    }
    
    func showToast(_ message : String, duration: TimeInterval = ToastManager.shared.duration, position: ToastPosition = .center) {
    
        self.view.makeToast(message, duration: duration, position: position)
    }
    
    func gotoNavVC (_ nameVC: String) {
        
        let toVC = self.storyboard?.instantiateViewController( withIdentifier: nameVC)
        self.navigationController?.pushViewController(toVC!, animated: true)
    }
    
    func doDismiss(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func gotoVC(_ nameVC: String){
        
        let toVC = self.storyboard?.instantiateViewController( withIdentifier: nameVC)
        toVC!.modalPresentationStyle = .fullScreen
        self.present(toVC!, animated: false, completion: nil)
    }
    
    func progressHUD(view : UIView, mode: MBProgressHUDMode = .annularDeterminate) -> MBProgressHUD {
    
    let hud = MBProgressHUD .showAdded(to:view, animated: true)
    hud.mode = mode
    hud.label.text = "Loading";
    hud.animationType = .zoomIn
    hud.tintColor = UIColor.white
    hud.contentColor = UIColor(named: "PrimaryColor")
    return hud
    }
    
    func gotoMainVC() {
       
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let menuVC: MenuVC = mainStoryboard.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
        let centerNavVC = mainStoryboard.instantiateViewController(withIdentifier: "HomeNav") as! UINavigationController
        
        let rootController = FAPanelController()
        rootController.configs.leftPanelWidth = 100
        rootController.configs.bounceOnRightPanelOpen = false
        _ = rootController.center(centerNavVC).left(menuVC)
        rootController.leftPanelPosition = .front
        rootController.modalPresentationStyle = .fullScreen
        self.present(rootController, animated: true, completion: nil)

   }
    
//    func saveCartData() {
//        
//        // Save to local userdefault
//        let userDefaults = UserDefaults.standard
//        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: CartList)
//        userDefaults.set(encodedData, forKey: Constant.KEY_MYCART)
//        userDefaults.synchronize()
//    }
//    
//    func loadCartData() {
//        if let decoded = UserDefaults.standard.data(forKey: Constant.KEY_MYCART) {
//            let decodeCarts = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! [CartModel]
//            CartList = decodeCarts
//        }
//    }
}

extension DefaultsKeys {
    
    var userEmail : DefaultsKey<String?>{ return .init("userEmail")}
    var userPwd : DefaultsKey<String?>{ return .init("userPwd")}
    var userId : DefaultsKey<Int?>{ return .init("userId")}
    var userPhotoUrl : DefaultsKey<String?>{ return .init("userPhotoUrl")}
    var userName : DefaultsKey<String?>{ return .init("userName")}
    var userFirstName : DefaultsKey<String?>{ return .init("userFirstName")}
    var userLastName : DefaultsKey<String?>{ return .init("userLastName")}
    var userPhoneNumber : DefaultsKey<String?>{ return .init("userPhoneNumber")}
    var userLoginStaus : DefaultsKey<Bool>{ return .init("userLoginStaus", defaultValue: false)}
    var userLoginType : DefaultsKey<String?>{ return .init("userLoginType")}
    var userSocialId : DefaultsKey<String?>{ return .init("userSocialId")}
    var device_token : DefaultsKey<String?>{ return .init("device_token")}
    
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    convenience init(hex: String) {
        self.init(hex: hex, alpha:1)
    }
    
    convenience init(hex: String, alpha: CGFloat) {
        var hexWithoutSymbol = hex
        if hexWithoutSymbol.hasPrefix("#") {
            hexWithoutSymbol = hex.substring(1)
        }
        
        let scanner = Scanner(string: hexWithoutSymbol)
        var hexInt:UInt32 = 0x0
        scanner.scanHexInt32(&hexInt)
        
        var r:UInt32!, g:UInt32!, b:UInt32!
        switch (hexWithoutSymbol.length) {
        case 3: // #RGB
            r = ((hexInt >> 4) & 0xf0 | (hexInt >> 8) & 0x0f)
            g = ((hexInt >> 0) & 0xf0 | (hexInt >> 4) & 0x0f)
            b = ((hexInt << 4) & 0xf0 | hexInt & 0x0f)
            break;
        case 6: // #RRGGBB
            r = (hexInt >> 16) & 0xff
            g = (hexInt >> 8) & 0xff
            b = hexInt & 0xff
            break;
        default:
            // TODO:ERROR
            break;
        }
        
        self.init(
            red: (CGFloat(r)/255),
            green: (CGFloat(g)/255),
            blue: (CGFloat(b)/255),
            alpha:alpha)
    }
}

extension Float {
    func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
}

extension String {
    /**
     :name:    trim
     */
    public var trimmed: String {
        return trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    /**
     :name:    lines
     */
    public var lines: [String] {
        return components(separatedBy: CharacterSet.newlines)
    }
    
    /**
     :name:    firstLine
     */
    public var firstLine: String? {
        return lines.first?.trimmed
    }
    
    /**
     :name:    lastLine
     */
    public var lastLine: String? {
        return lines.last?.trimmed
    }
    
    /**
     :name:    replaceNewLineCharater
     */
    public func replaceNewLineCharater(separator: String = " ") -> String {
        return components(separatedBy: CharacterSet.whitespaces).joined(separator: separator).trimmed
    }
    
    /**
     :name:    replacePunctuationCharacters
     */
    public func replacePunctuationCharacters(separator: String = "") -> String {
        return components(separatedBy: CharacterSet.punctuationCharacters).joined(separator: separator).trimmed
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
     let tap: UITapGestureRecognizer =     UITapGestureRecognizer(target: self, action:    #selector(UIViewController.dismissKeyboard))
      tap.cancelsTouchesInView = false
      view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
       view.endEditing(true)
    }
}

