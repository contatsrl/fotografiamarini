//
//  Constant.swift
//  LAB1898
//
//  Created by cs on 2020/4/3.
//  Copyright © 2020 cs. All rights reserved.
//

import Foundation
import UIKit

class Constant {

    static let APPNAME                              = "LAB1898"
    static let OK                                   = "OK"
    static let CANCEL                               = "Cancel"
    static let NO                                   = "No"
    static let YES                                  = "Yes"
    
    //Image
    static let PLACEHOLDER_IMAGE                    = UIImage(named: "placeholder")
    static let AVATAR_IMAGE                         = UIImage(named: "profile")
    
    //error messages
    static let CHECK_FIRSTNAME_EMPTY                = "Please enter first name"
    static let CHECK_LASTNAME_EMPTY                 = "Please enter last name"
    static let CHECK_EMAIL_EMPTY                    = "Please enter your email address"
    static let CHECK_VAILD_EMAIL                    = "Please enter valid email"
    static let CHECK_PHONE_EMPTY                    = "Please enter your phone number"
    static let CHECK_PASSWORD                       = "Please enter password"
    static let CONFIRM_PASSWORD                     = "Please confirm your password"
    static let CHECK_REMOVE_ORDER                   = "Are you sure remove this product?"
    

    static let WARNING                              = "Warning!"
    static let MSG_SUCCESS                          = "Success"
    static let MSG_FAIL                             = "Fail"
    static let ERROR_CONNECT                        = "The connection to the server failed"
    static let MSG_SOMETHING_WRONG                  = "Something went wrong"
    
    // Request Params
    
    // Response parameters
    static let RES_MESSAGE                          = "message"
    static let DATA                                 = "data"
    static let USER_ID                              = "ID"

    
    // Key
    static let KEY_MYCART                           = "k_cart"
    
    
    // Result code
    static let RESULT_CODE                          = "status"
    static let CODE_404                             = 404
    static let CODE_400                             = 400
    static let CODE_SUCCESS                         = "success"
    static let CODE_FAIL                            = "error"
    
    //Logout
    static let CONFIRM_LOGOUT                       = "Are you sure logout?"
}

