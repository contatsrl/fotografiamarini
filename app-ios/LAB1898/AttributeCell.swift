//
//  AttributeCell.swift
//  LAB1898
//
//  Created by cs on 2020/4/20.
//  Copyright © 2020 cs. All rights reserved.
//

import UIKit
import iOSDropDown

class AttributeCell: UICollectionViewCell {
  
    @IBOutlet weak var attributeSelectField: DropDown!
    @IBOutlet weak var attributeNameLabel: UILabel!
    var attributeSelectFieldIds = [Int]()
    
    var entity : AttributeModel! {
        didSet{
            attributeNameLabel.text = entity.name
            
            attributeSelectField.optionArray = entity.options
            attributeSelectField.placeholder = "Choose \(entity.name)"
            for i in 0 ..< entity.options.count {
                attributeSelectFieldIds.append(i)
            }
            attributeSelectField.optionIds = attributeSelectFieldIds
            attributeSelectField.isSearchEnable = false
            attributeSelectField.selectedRowColor = .clear
            attributeSelectField.rowHeight = 40
            attributeSelectField.checkMarkEnabled = false
            attributeSelectField.arrowColor = UIColor(named: "PrimaryColor")!
            attributeSelectField.didSelect(completion: { (selected, index, id)  in
                ///self.dimensionDropdown.isSearchEnable = true
                attributeValues[self.attributeSelectField.tag]  = selected
                if self.attributeNameLabel.text == "Dimensioni" {
                    product_dimension = selected
                }
            })
            
            attributeSelectField.text = ""
        }
    }
    
}
