//
//  ApiManager.swift
//  LAB1898
//
//  Created by cs on 2020/4/9.
//  Copyright © 2020 cs. All rights reserved.
//

import Foundation
import SwiftyJSON
import SwiftyUserDefaults
import Alamofire

let HOST = "https://lab1898.socialtab.it/"
let AUTH = HOST + "index.php/wp-json/app/v1/"

let API = HOST + "index.php/wp-json/wc/v3/"
let GETALLCATEGORY = API + "products/categories"
let GETALLPRODUCT = API + "products"
let GETORDER = API + "orders"
let SIGNUP = AUTH + "users/"
let LOGIN = SIGNUP + "auth"




let USERNAME = "ck_49ed700e19dcaca93451f0043cfaf77b550fe202"
let PASSWORD = "cs_5be2f2a12ad16a7a2ef9031b44ceb65df8e3087d"

let credentialData = "\(USERNAME):\(PASSWORD)".data(using: String.Encoding.utf8)!
let base64Credentials = credentialData.base64EncodedString()

let multipartFormDataEncodingMemoryThreshold: UInt64 = 10_000_000
var headers: HTTPHeaders = ["Authorization": "Basic \(base64Credentials)"]

let session = Alamofire.Session()

class ApiManager : NSObject {
    
    class func getAllCategory(completion: @escaping(_ success:Bool, _ response: Any?)-> ()){
               
        session.request(GETALLCATEGORY, method: .get, headers: headers).responseJSON{ response in
            switch response.result {
                
            case .failure:
                completion(false, nil)
                
            case .success(let data):
                completion(true, data)
            }
        }
    }
    
    class func getAllProducts(_ per_page: Int, page: Int, completion: @escaping(_ success:Bool, _ response: Any?)-> ()){
       
        session.request(GETALLPRODUCT + "?per_page=\(per_page)&page=\(page)", method: .get, headers: headers).responseJSON{ response in
            switch response.result {
                
            case .failure:
                completion(false, nil)
                
            case .success(let data):
                completion(true, data)
            }
        }
    }
    
    class func getProduct(productId: Int, completion: @escaping(_ success:Bool, _ response: Any?)-> ()){
       
        let URL = GETALLPRODUCT + "/" + "\(productId)"
        session.request(URL, method: .get, headers: headers).responseJSON{ response in
            switch response.result {
                
            case .failure:
                completion(false, nil)
                
            case .success(let data):
                completion(true, data)
            }
        }
    }
    
    class func signup(email: String, password: String, firstName: String, lastName: String, phoneNumber: String, completion: @escaping(_ success:Bool, _ response: Any?)-> ()){
        
        let params = ["email": email,
                      "pass": password,
                      "pass_confirm": password,
                      "first_name": firstName,
                      "last_name": lastName,
                      "phone": phoneNumber
                    ]
        
        session.request(SIGNUP, method: .post, parameters: params).responseJSON{ response in
            switch response.result {
                
            case .failure:
                completion(false, nil)
                
            case .success(let data):
                let dict = JSON(data)
                let result = dict[Constant.RESULT_CODE].stringValue
                
                if result == Constant.CODE_SUCCESS {
                    completion(true, data)
                } else {
                    completion(false, data)
                }
                            
            }
        }
    }
    
    class func login(email: String, password: String, device: String, completion: @escaping(_ success:Bool, _ response: Any?)-> ()){
        
        let params = ["email": email,
                      "pass": password,
                      "device": device
                    ]
        
        session.request(LOGIN, method: .post, parameters: params).responseJSON{ response in
            switch response.result {
                
            case .failure:
                completion(false, nil)
                
            case .success(let data):
                let dict = JSON(data)
                let result = dict[Constant.RESULT_CODE].stringValue
                
                if result == Constant.CODE_SUCCESS {
                    completion(true, data)
                } else {
                    completion(false, data)
                }
                            
            }
        }
    }
    
    class func socialLogin(name: String, email: String, social_id: String, photo_url: String, login_type: String, completion: @escaping(_ success:Bool, _ response: Any?)-> ()){
        
        let SOCIALLOGIN = SIGNUP + "socialauth/"
        let params = ["name": name,
                      "email": email,
                      "social_id": social_id,
                      "photo_url": photo_url,
                      "login_type": login_type
                    ]
        
        session.request(SOCIALLOGIN, method: .post, parameters: params).responseJSON{ response in
            switch response.result {
                
            case .failure:
                completion(false, nil)
                
            case .success(let data):
                let dict = JSON(data)
                let result = dict[Constant.RESULT_CODE].stringValue
                
                if result == Constant.CODE_SUCCESS {
                    completion(true, data)
                } else {
                    completion(false, data)
                }
                            
            }
        }
    }
    
    class func getUserData(userId: Int, completion: @escaping(_ success:Bool, _ response: Any?)-> ()){
       
        let URL = AUTH + "user/" + "\(userId)"
        session.request(URL, method: .get).responseJSON{ response in
            switch response.result {
                
            case .failure:
                completion(false, nil)
                
            case .success(let data):
                let dict = JSON(data)
                let result = dict[Constant.RESULT_CODE].stringValue
                
                if result == Constant.CODE_SUCCESS {
                    completion(true, data)
                } else {
                    completion(false, data)
                }
            }
        }
    }
    
    class func updateprofile(userId: Int, firstName: String, lastName: String, phoneNumber: String,email: String, photo: UIImage, completion: @escaping(_ success:Bool, _ response: Any?)-> ()){
        
        let URL = AUTH + "user/" + "\(userId)"
        let params = ["meta[first_name]": firstName,
                      "meta[last_name]": lastName,
                      "meta[phone]": phoneNumber,
                      "user_email": email  
                    ]
        let token = Defaults[\.device_token]!
        let header: HTTPHeaders = ["Authorization": token]
        
        let upload = session.upload(
            multipartFormData: { (multipartFormData) in
                let data = photo.jpegData(compressionQuality: 0.8)
                multipartFormData.append(data!, withName: "meta[avatar][]", fileName: "photo" + ".jpg", mimeType: "image/jpeg")
                for (key, value) in params {
                    if let data = value.data(using: .utf8) {
                        multipartFormData.append(data, withName: key)
                    }
                }
            },
            to: URL,
            usingThreshold: multipartFormDataEncodingMemoryThreshold,
            method: .post,
            headers: header,
            interceptor: nil,
            fileManager: FileManager.default)
        
        upload.responseJSON { (response) in
            print("multipart response", response)
            
            switch response.result {
                
            case .failure:
                completion(false, nil)
                
            case .success(let data):
                let dict = JSON(data)
                print("dict", dict)
                let result = dict[Constant.RESULT_CODE].stringValue
               
                if result == "success" {
                    completion(true, data)
                } else {
                    completion(false, data)
                }
                            
            }
        }
    }
    
    class func getOrder(completion: @escaping(_ success:Bool, _ response: Any?)-> ()){
        
        session.request(GETORDER, method: .get, headers: headers).responseJSON{ response in
            
            switch response.result {
                
            case .failure:
                completion(false, nil)
                
            case .success(let data):
                completion(true, data)
            }
        }
    }
    
    class func reoveryPassword(email: String, completion: @escaping(_ success:Bool, _ response: Any?)-> ()){
        
        let RECOVERYPASSWORD = SIGNUP + "recover/"
        
        let params = ["email": email]
        
        session.request(RECOVERYPASSWORD, method: .post, parameters: params).responseJSON{ response in
            switch response.result {
                
            case .failure:
                completion(false, nil)
                
            case .success(let data):
                let dict = JSON(data)
                let result = dict[Constant.RESULT_CODE].stringValue
                
                if result == Constant.CODE_SUCCESS {
                    completion(true, data)
                } else {
                    completion(false, data)
                }
                            
            }
        }
    }
    
    class func getVariation(product_id: Int, per_page: Int, page: Int, completion: @escaping(_ success:Bool, _ response: Any?)-> ()){
       
        let URL = GETALLPRODUCT + "/\(product_id)" + "/variations?per_page=\(per_page)&page=\(page)"
        session.request(URL, method: .get, headers: headers).responseJSON{ response in
            switch response.result {
                
            case .failure:
                completion(false, nil)
                
            case .success(let data):
                completion(true, data)
            }
        }
    }
}

