//
//  PageIndicatorCell.swift
//  LAB1898
//
//  Created by cs on 2020/4/3.
//  Copyright © 2020 cs. All rights reserved.
//

import UIKit

class PageIndicatorCell: UICollectionViewCell {
    
    @IBOutlet weak var lblPageNumber: UILabel!
}
