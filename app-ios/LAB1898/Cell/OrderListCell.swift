//
//  OrderListCell.swift
//  LAB1898
//
//  Created by cs on 2020/4/6.
//  Copyright © 2020 cs. All rights reserved.
//

import UIKit

class OrderListCell: UICollectionViewCell {
    
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var editImageButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var productCountLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var editImageButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var tiTleLabelConstraint: NSLayoutConstraint!
    
    var entity : TestCartModel! {
        didSet{
            productName.text = entity.product_name
            productPriceLabel.text = "€ \(String(format: "%.2f", entity.product_price))"            
        }
    }
    
}
