//
//  ImageUploadCell.swift
//  LAB1898
//
//  Created by cs on 2020/4/3.
//  Copyright © 2020 cs. All rights reserved.
//

import UIKit
import ImageScrollView

class ImageUploadCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var real_heightLabel: UILabel!
    @IBOutlet weak var real_widthLabel: UILabel!
    @IBOutlet weak var placeholderLabel: UILabel!
    var crop_width: Float = 0
    var crop_height: Float = 0
    
    var cropImageBlock: (() -> Void)?
    var addImageBlock: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    @IBAction func btnAddImageBlock(_ sender: Any) {
        
        if let addImageBlock = addImageBlock {
            addImageBlock()
        }
    }
    
    @IBAction func btncropImageBlock(_ sender: Any) {
        
        if let cropImageBlock = cropImageBlock {
            cropImageBlock()
        }
    }
    
    var entity : editImageModel! {
           
        didSet{
            photoImageView.image = entity.imgFile
            real_widthLabel.text = entity.real_width
            real_heightLabel.text = entity.real_height
            crop_width = entity.width
            crop_height = entity.height
        }
    }
}

