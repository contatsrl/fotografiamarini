//
//  CategoryCell.swift
//  LAB1898
//
//  Created by cs on 2020/4/3.
//  Copyright © 2020 cs. All rights reserved.
//

import UIKit
import SDWebImage

class CategoryCell: UITableViewCell {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var imageCountLabel: UILabel!
    @IBOutlet weak var imageCountView: UIView!
    @IBOutlet weak var nameView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var entity : ProductModel! {
        
        didSet{ 
            productImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
            productImageView.sd_setImage(with: URL(string: entity.productImages.count > 0 ? entity.productImages[0] : ""), placeholderImage: Constant.PLACEHOLDER_IMAGE)
            productNameLabel.text = entity.productName
            productPriceLabel.text = "da \(String(format: "%.2f", entity.productPrice))€"
            imageCountLabel.text = entity.imageCountString
            
         
            if entity.imageCountString == "" {
                imageCountView.isHidden = true
            } else {
                imageCountView.isHidden = false
            }
        }
    }

}
