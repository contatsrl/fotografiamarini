//
//  CategoryListCell.swift
//  LAB1898
//
//  Created by cs on 2020/4/3.
//  Copyright © 2020 cs. All rights reserved.
//

import UIKit
import SDWebImage

class CategoryListCell: UITableViewCell {

    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var categoryPriceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var entity : CategoryModel! {
        
        didSet{
            
            categoryImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
            categoryImageView.sd_setImage(with: URL(string: entity.categoryImage), placeholderImage: Constant.PLACEHOLDER_IMAGE)
            categoryNameLabel.text = entity.categoryName
            categoryPriceLabel.text = entity.categoryStartPrice
        }
    }
}
