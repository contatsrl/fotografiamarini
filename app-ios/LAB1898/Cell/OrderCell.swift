//
//  OrderCell.swift
//  LAB1898
//
//  Created by cs on 2020/4/6.
//  Copyright © 2020 cs. All rights reserved.
//

import UIKit

class OrderCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var orderCountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var entity : OrderProductModel! {
        didSet{
            titleLabel.text = entity.productName
            priceLabel.text = "\(entity.productOrderCount)"
            orderCountLabel.text = "x \(entity.productPrice)"
        }
    }
}
