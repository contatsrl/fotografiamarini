//
//  ForgotVC.swift
//  LAB1898
//
//  Created by cs on 2020/4/2.
//  Copyright © 2020 cs. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftyJSON
import SwiftyUserDefaults
import MBProgressHUD

class ForgotVC: BaseViewController {

    @IBOutlet weak var tfEmail: SkyFloatingLabelTextField!
    var hud : MBProgressHUD!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    @IBAction func onClickSend(_ sender: Any) {
        
        let email = tfEmail.text!
        self.hud = self.progressHUD(view: self.view, mode: .indeterminate)
        self.hud.show(animated: true)
        
        ApiManager.reoveryPassword(email: email, completion: {(isSuccess,data) in

            self.hud.hide(animated: true)
            if isSuccess{
                self.doDismiss()
            } else {
                if data == nil {
                    self.showToast(Constant.ERROR_CONNECT, duration: 2, position: .bottom)
                } else {
                    let dict = JSON(data as Any)
                    let result = dict[Constant.RESULT_CODE].stringValue
                    let message = dict[Constant.RES_MESSAGE].stringValue
                    if result == "error" {
                        self.showToast("\(message)", duration: 2, position: .bottom)
                    }
                    else {
                        self.showToast("Something went worng.", duration: 2, position: .bottom)
                    }
                }
            }
        })
    }
    
    @IBAction func onClickLogin(_ sender: Any) {
        
        doDismiss()
    }
}
