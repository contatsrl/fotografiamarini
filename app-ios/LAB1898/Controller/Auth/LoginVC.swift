//
//  LoginVC.swift
//  LAB1898
//
//  Created by cs on 2020/4/2.
//  Copyright © 2020 cs. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftyUserDefaults
import SwiftyJSON
import Firebase
import GoogleSignIn
import FBSDKLoginKit
import MBProgressHUD

class LoginVC: BaseViewController {

    @IBOutlet weak var tfEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var tfPassword: SkyFloatingLabelTextField!
    
    var email = ""
    var password = ""
    var SocialId = ""
    var name = ""
    var loginType = ""
    var photo_url = ""
    var hud : MBProgressHUD!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Defaults[\.userLoginStaus] {
            
            loginType = Defaults[\.userLoginType]!
            if loginType == LoginType.email.rawValue {
                email = Defaults[\.userEmail]!
                password = Defaults[\.userPwd]!
                onCallLogin()
            } else {
                email = Defaults[\.userEmail]!
                SocialId = Defaults[\.userSocialId]!
                name = Defaults[\.userName]!
                photo_url = Defaults[\.userPhotoUrl]!
                doSocialLogin(loginType)
            }
        }
    }
    
    @IBAction func onClickLogin(_ sender: Any) {
        
        email = tfEmail.text!
        password = tfPassword.text!
        
        checkValidate()
    }
    
    func checkValidate() {
        
        if email.isEmpty {
            showToast(Constant.CHECK_EMAIL_EMPTY)
            return
        }
        
        if !isValidEmail(email: email) {
            showToast(Constant.CHECK_VAILD_EMAIL)
            return
        }
        
        if password.isEmpty {
            showToast(Constant.CHECK_PASSWORD)
            return
        }
        
        onCallLogin()
    }
    
    func onCallLogin() {
        
        self.hud = self.progressHUD(view: self.view, mode: .indeterminate)
        self.hud.show(animated: true)
        
        ApiManager.login(email: email, password: password, device: "", completion: {(isSuccess,data) in

            self.hud.hide(animated: true)
            if isSuccess{

                let dict = JSON(data!)
                let userData = dict[Constant.DATA]
                let userId = userData["user_id"].intValue
                Defaults[\.userId] = userId
                Defaults[\.userEmail] = self.email
                Defaults[\.userPwd] = self.password
                Defaults[\.userLoginStaus] = true
                Defaults[\.userLoginType] = LoginType.email.rawValue
                Defaults[\.device_token] = userData["authtoken"].stringValue
                self.gotoMainVC()
            }
            else{
                if data == nil {
                    self.showToast(Constant.ERROR_CONNECT, duration: 2, position: .bottom)
                } else {
                    let dict = JSON(data as Any)
                    let result = dict[Constant.RESULT_CODE].stringValue
                    let message = dict[Constant.RES_MESSAGE].stringValue
                    if result == "error" {
                        self.showToast("\(message)", duration: 2, position: .bottom)
                    }
                    else {
                        self.showToast("Login Failed.", duration: 2, position: .bottom)
                    }
                }
            }
        })
    }
    
    @IBAction func onClickFaceBookLogin(_ sender: Any) {
        
        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logIn(permissions: ["public_profile", "email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!
                if(fbloginresult.grantedPermissions.contains("email")) {
                    self.getFBUserData()
                    fbLoginManager.logOut()
                }
            } else {
                print("Login failed")
            }
        }
    }
    
    @IBAction func onClickGoogleLogin(_ sender: Any) {
        
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func onClickSignup(_ sender: Any) {
        
        gotoNavVC("SignupVC")
    }
    
    func getFBUserData(){
        
        if((AccessToken.current) != nil) {
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, email, first_name, last_name"]).start(completionHandler: { (connection, result, error) -> Void in
                
                if (error == nil) {
                    
                    let jsonResponse = JSON(result!)
                    
                    print("jsonResponse", jsonResponse)
                    
                    let id = jsonResponse["id"].stringValue
                    self.password = id
                    Defaults[\.userSocialId] = id
                    self.email = jsonResponse["email"].string ?? String(format: "%@@facebook.com", id)
                    Defaults[\.userEmail] = self.email
                    Defaults[\.userName] = jsonResponse["name"].string ?? "unknown"
                    Defaults[\.userPhotoUrl] = "https://graph.facebook.com/" + id + "/picture?type=large"
                    Defaults[\.userFirstName] = jsonResponse["first_name"].string ?? "unknown"
                    Defaults[\.userLastName] = jsonResponse["last_name"].string ?? "unknown"
                    print("FB result", self.password, self.email, Defaults[\.userPhotoUrl]!, id)
                    
                    self.doSocialLogin(LoginType.facebook.rawValue)
                    
                } else {
                    // TODO:  Exeption  ****
                    print(error!)
                }
            })
        } else {
            print("token is null")
        }
    }
    
    func doSocialLogin(_ social_type: String) {

        let login_type = social_type
        Defaults[\.userLoginType] = login_type
        
        self.hud = self.progressHUD(view: self.view, mode: .indeterminate)
        self.hud.show(animated: true)
        
        ApiManager.socialLogin(name: Defaults[\.userName]!, email: email, social_id: password, photo_url: Defaults[\.userPhotoUrl]!, login_type: login_type, completion: {(isSuccess,data) in

            self.hud.hide(animated: true)
            if isSuccess{
                
                let dict = JSON(data!)
                let userData = dict[Constant.DATA]
                let userId = userData["user_id"].intValue
                Defaults[\.userId] = userId
                Defaults[\.userLoginStaus] = true
                Defaults[\.device_token] = userData["authtoken"].stringValue
            
                self.gotoMainVC()
            }
            else{
                if data == nil {
                    self.showToast(Constant.ERROR_CONNECT, duration: 2, position: .bottom)
                } else {
                    let dict = JSON(data as Any)
                    let result = dict[Constant.RESULT_CODE].stringValue
                    let message = dict[Constant.RES_MESSAGE].stringValue
                    if result == "error" {
                        self.showToast("\(message)", duration: 2, position: .bottom)
                    }
                    else {
                        self.showToast("Login Failed.", duration: 2, position: .bottom)
                    }
                }
            }
        })
    }
    
    @IBAction func onClickForgot(_ sender: Any) {
        
        gotoNavVC("ForgotVC")
    }
}
//MARK:--GoogleLogin Extension
extension LoginVC: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
      
        if error == nil {
            
            guard let user = user else {return}
            print("GIDSignIn User", user)
            
            let id = user.userID!
            self.email = user.profile.email ?? String(format: "%@@google.com", id)
            Defaults[\.userEmail] = self.email
            Defaults[\.userSocialId] = id
            Defaults[\.userName] = user.profile.name ?? "unknown"
            Defaults[\.userFirstName] = user.profile.familyName ?? "unknown"
            Defaults[\.userLastName] = user.profile.givenName ?? "unknown"
            Defaults[\.userPhotoUrl] = user.profile.imageURL(withDimension: 300)!.description
            
            print("Google result", self.password, self.email, id, Defaults[\.userPhotoUrl]!)
            doSocialLogin(LoginType.google.rawValue)
        } else {
            print("GIDSignIn Error:--  ", error!.localizedDescription)
        }
    }

    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        viewController.modalPresentationStyle = .fullScreen
        present(viewController, animated: true) {}
    }
}
