//
//  SignupVC.swift
//  LAB1898
//
//  Created by cs on 2020/4/2.
//  Copyright © 2020 cs. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftyJSON
import SwiftyUserDefaults
import MBProgressHUD


class SignupVC: BaseViewController {
    
    @IBOutlet weak var tfFirstName: SkyFloatingLabelTextField!
    @IBOutlet weak var tfLastName: SkyFloatingLabelTextField!
    @IBOutlet weak var tfPhoneNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var tfEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var tfPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var tfConfirmPassword: SkyFloatingLabelTextField!
    
    var firstName = ""
    var lastName = ""
    var phone = ""
    var email = ""
    var password = ""
    var confirmPassword = ""
    var hud: MBProgressHUD!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func onClickSignup(_ sender: Any) {
        
        firstName = tfFirstName.text!
        lastName = tfLastName.text!
        phone = tfPhoneNumber.text!
        email = tfEmail.text!
        password = tfPassword.text!
        confirmPassword = tfConfirmPassword.text!
        
        checkValidate()
    }
    
    @IBAction func onClickRecovery(_ sender: Any) {
    }
    
    @IBAction func gotoLogin(_ sender: Any) {
        
        doDismiss()
    }
    
    func checkValidate() {
        
        if firstName.isEmpty {
            showToast(Constant.CHECK_FIRSTNAME_EMPTY)
            return
        }
        if lastName.isEmpty {
            showToast(Constant.CHECK_LASTNAME_EMPTY)
            return
        }
        if phone.isEmpty {
            showToast(Constant.CHECK_PHONE_EMPTY)
            return
        }
        if password.isEmpty {
            showToast(Constant.CHECK_PASSWORD)
            return
        }
        if email.isEmpty {
            showToast(Constant.CHECK_EMAIL_EMPTY)
            return
        }
        if !isValidEmail(email: email) {
            showToast(Constant.CHECK_VAILD_EMAIL)
            return
        }
        if confirmPassword.isEmpty {
            showToast(Constant.CONFIRM_PASSWORD)
            return
        }
        if password != confirmPassword {
            showToast(Constant.CONFIRM_PASSWORD)
            return
        }
        onCallSignup()
    }
    
    func onCallSignup() {
        
        self.hud = self.progressHUD(view: self.view, mode: .indeterminate)
        self.hud.show(animated: true)
        
        ApiManager.signup(email: email, password: password, firstName: firstName, lastName: lastName, phoneNumber: phone, completion: {(isSuccess,data) in

            self.hud.hide(animated: true)
            if isSuccess{
                
                let dict = JSON(data!)
                let userData = dict[Constant.DATA]
                let userId = userData["id"].intValue
                Defaults[\.device_token] = userData["authtoken"].stringValue
                Defaults[\.userId] = userId
                Defaults[\.userEmail] = self.email
                Defaults[\.userFirstName] = self.firstName
                Defaults[\.userLastName] = self.lastName
                Defaults[\.userPwd] = self.password
                Defaults[\.userPhoneNumber] = self.phone
                Defaults[\.userLoginStaus] = true
                self.gotoMainVC()
            }
            else{
                if data == nil {
                    self.showToast(Constant.ERROR_CONNECT, duration: 2, position: .bottom)
                } else {
                    let dict = JSON(data as Any)
                    let result = dict[Constant.RESULT_CODE].stringValue
                    let message = dict[Constant.RES_MESSAGE].stringValue
                    if result == "error" {
                        self.showToast("\(message)", duration: 2, position: .bottom)
                    }
                    else {
                        self.showToast("Something is wrong.", duration: 2, position: .bottom)
                    }
                }
            }
        })
    }
    @IBAction func onClickTerms(_ sender: Any) {
        
        if let url = URL(string: "https://lab1898.socialtab.it/index.php/privacy-policy/") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func onClickForgot(_ sender: Any) {
        
        gotoNavVC("ForgotVC")
    }
    
}
