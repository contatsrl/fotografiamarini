//
//  MenuVC.swift
//  LAB1898
//
//  Created by cs on 2020/4/4.
//  Copyright © 2020 cs. All rights reserved.
//

import UIKit


class MenuVC: BaseViewController {

    @IBOutlet weak var navigationBarHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        navigationBarHeight.constant = 44 + (window?.safeAreaInsets.top)!
        
        
    }
    
    @IBAction func onClickProfile(_ sender: Any) {
        
        gotoMenuVC("ProfileNav")
    }
    
    @IBAction func onClickProduct(_ sender: Any) {
        
        gotoMenuVC("HomeNav")
    }
    
    @IBAction func onClickContact(_ sender: Any) {
        
        loadingPage = "contact"
        gotoMenuVC("WebNav")
    }
    
    @IBAction func onClickWebsite(_ sender: Any) {
        
        loadingPage = "website"
        gotoMenuVC("WebNav")
    }
    
    @IBAction func onClickHistory(_ sender: Any) {
        
        loadingPage = "history"
        gotoMenuVC("WebNav")
    }
    @IBAction func onClickMyOrderHistory(_ sender: Any) {
        
        loadingPage = "orderHistory"
        gotoMenuVC("WebNav")
    }
    
    @IBAction func onClickLogout(_ sender: Any) {
        
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        
        let loginNavVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginNav") as! UINavigationController
        UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.rootViewController = loginNavVC
    }
    
    func gotoMenuVC(_ navVCName: String) {

        let centerNavVC = storyboard?.instantiateViewController(withIdentifier: navVCName) as! UINavigationController
        panel!.configs.bounceOnCenterPanelChange = true
        
        panel!.center(centerNavVC, afterThat: {
            print("Executing block after changing center panelVC From Left Menu")
        })
    }
}
