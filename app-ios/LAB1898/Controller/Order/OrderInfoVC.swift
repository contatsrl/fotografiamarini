//
//  OrderInfoVC.swift
//  LAB1898
//
//  Created by cs on 2020/4/6.
//  Copyright © 2020 cs. All rights reserved.
//

import UIKit
import SwiftyJSON
import MBProgressHUD


struct OrderInfo: Codable {
    var title = ""
    var price = ""
}

class OrderInfoVC: BaseViewController {

    @IBOutlet weak var tblOrderInfo: UITableView!
    
    var sectionTitles = ["USEERS DATA", "Name", "Family Name", "E-mail", "Phone", "SHIPPING METHODS", "Method name", "Address", "Name", "PAYMENT METHODS", "Method name", "DATI ORDINE", "Order sent", "Order accepted"]
    
    var values = ["", "Slavatar", "Marini", "slavtor.marini@mail.com", "329655854", "", "Consegna a domiclio", "Via Roma 33", "Mari Rossi", "", "Carta di credito", "", "27/02/2020 19:21", "27/02/2020 19:26"]
    
    var dataSource = [OrderProductModel]()
    var orderData = [OrderInfoModel]()
    var hud : MBProgressHUD!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Order Information"
        getOrder()
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        
        CartList.removeAll()       
//        saveCartData()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    fileprivate func getOrder() {
        
        dataSource.removeAll()
        orderData.removeAll()
        
        self.hud = self.progressHUD(view: self.view, mode: .indeterminate)
        self.hud.show(animated: true)
       
        ApiManager.getOrder{ (isSuccess, data) in
               
            self.hud.hide(animated: true)
            
            if isSuccess {
               
                let dict = JSON(data!)
                
                let orderInfos = dict.arrayValue
                for item in orderInfos {
                    let orderInformation = OrderInfoModel(dict: item)
                    self.orderData.append(orderInformation)
                    for product in orderInformation.orderedProduct {
                        self.dataSource.append(product)
                    }
                }
                self.tblOrderInfo.reloadData()
            } else {
                self.showToast("Something went worng\nTry again later")
            }
        }
    }
}

extension OrderInfoVC: UITableViewDelegate, UITableViewDataSource {
    // MARK: - Table View Data Source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
        return 1 + dataSource.count + 14
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell")
            return cell!
            
        } else if indexPath.row == dataSource.count + 1 || indexPath.row == dataSource.count + 6 ||
            indexPath.row == dataSource.count + 10 ||
            indexPath.row == dataSource.count + 12 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "TitleLabelCell") as! TitleLabelCell
            cell.lblTitle.text = sectionTitles[indexPath.row - 1 - dataSource.count]
            return cell
        
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell") as! OrderCell
            
            if indexPath.row - 1 < dataSource.count {
                let order = dataSource[indexPath.row-1]
                cell.titleLabel.text = order.productName
                cell.priceLabel.text = "\(order.productPrice)"
                cell.orderCountLabel.text = "\(order.productOrderCount)"
            } else {
                
                cell.titleLabel.text = sectionTitles[indexPath.row - 1 - dataSource.count]
                cell.priceLabel.text = values[indexPath.row - 1 - dataSource.count]
            }
            return cell            
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            return 120
            
        } else if indexPath.row == dataSource.count + 1 || indexPath.row == dataSource.count + 6 ||
            indexPath.row == dataSource.count + 10 ||
            indexPath.row == dataSource.count + 12 {
            
            return 70
        
        } else {
            
            return 30
            
        }
    }
}
