//
//  CheckoutResultVC.swift
//  LAB1898
//
//  Created by cs on 2020/4/13.
//  Copyright © 2020 cs. All rights reserved.
//

import UIKit
import WebKit

class CheckoutResultVC: BaseViewController, WKNavigationDelegate {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var indicatorView: UIView!
    
    var resultUrl = ""
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backBarButtton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        backBarButtton.tintColor = .white
        navigationItem.backBarButtonItem = backBarButtton
        
        self.title = "Checkout"   

        webView.navigationDelegate = self
        indicatorView.isHidden = false
        loadingIndicator.startAnimating()
        let url = URL(string: resultUrl)
        webView.load(URLRequest(url: url!))
        
       
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        indicatorView.isHidden = false
        loadingIndicator.isHidden = false
        loadingIndicator.startAnimating()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        loadingIndicator.stopAnimating()
        indicatorView.isHidden = true
        loadingIndicator.isHidden = true
    }
}
