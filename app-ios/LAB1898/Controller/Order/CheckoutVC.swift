//
//  CheckoutVC.swift
//  LAB1898
//
//  Created by cs on 2020/4/5.
//  Copyright © 2020 cs. All rights reserved.
//

import UIKit
import SwiftyJSON
import SwiftyUserDefaults
import Alamofire

var selectedIndex = 0
class CheckoutVC: BaseViewController {

    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var orderProductList: DynamicHeightCollectionView!
    @IBOutlet weak var progressBgView: UIView!
    @IBOutlet weak var progressView: MKMagneticProgress!
    
    var cartProduct = [ProductModel]()
    var cartData = [TestCartModel]()
    var cartProductIds = [Int]()
    var totalPrice : Float = 0.0
    
    var countTimer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let backBarButtton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        backBarButtton.tintColor = .white
        navigationItem.backBarButtonItem = backBarButtton
        self.title = "My Cart"
//        loadCartData()
        
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        progressBgView.isHidden = true
        progressView.setProgress(progress: 0)
        for cart in CartList {
            cartProductIds.append(cart.product_id)
        }
//        loadCartData()
        calculateTotalPrice()
        loadOrderProductList()
    }
    
    func initView() {
        
        progressBgView.isHidden = true
        progressView.orientation = Orientation.top
        progressView.lineCap = .square
    }

    func loadOrderProductList() {
        
        for item in CartList {
            self.cartData.append(item)
        }
        orderProductList.reloadData()
        
//        for product in ProductList {
//            if cartProductIds.contains(product.id) {
//                self.cartProduct.append(product)
//            }
//        }
//        orderProductList.reloadData()
    }
    
    @objc func onMinus(_ sender: UIButton) {
        totalPrice = Float(totalPriceLabel.text!.dropFirst())!
        let cart = CartList[sender.tag]
        if cart.product_count == 1 {
            showAlert(title: Constant.WARNING, message: Constant.CHECK_REMOVE_ORDER, positive: Constant.YES, negative: Constant.CANCEL){
                CartList.remove(at: sender.tag)
                // Save to local userdefault
//                self.saveCartData()
                self.orderProductList.reloadData()
            }
        } else {
            CartList[sender.tag].product_count -= 1
            // Save to local userdefault
//            saveCartData()
            calculateTotalPrice()
            orderProductList.reloadData()
        }        
    }
    
    @objc func onPlus(_ sender: UIButton) {
        
        CartList[sender.tag].product_count += 1
        // Save to local userdefault
//        saveCartData()
        calculateTotalPrice()
        orderProductList.reloadData()
    }
    
    func calculateTotalPrice() {
        totalPrice = 0
        for item in CartList {
            totalPrice += item.product_price * Float(item.product_count)
        }
        totalPriceLabel.text = "€\(String(format: "%.2f", totalPrice))"
    }
    
    @IBAction func onClickBuy(_ sender: Any) {
        
        if CartList.isEmpty {
            showToast("You didn't select anything.")
            return
        }
        
        for (index, item) in CartList.enumerated() {
            if item.productImagesData.isEmpty {
                showToast("You didn't add images in \(index + 1)th order item.")
                return
            }
        }
        onCallCheckoutApi()
    }
    
    func onCallCheckoutApi() {
        
        progressBgView.isHidden = false
       
        var timeCount = 0
        countTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (timer) in
            timeCount += 1
            self.progressView.setProgress(progress: CGFloat(Double(timeCount)/150.0), animated: false)
            if timeCount == 150 {
                self.countTimer!.invalidate()
            }
        }
        
        createOrder(cart: CartList, completion: {(isSuccess,data) in

            if isSuccess {

                let dict = JSON(data!)
                let url = dict["data"].stringValue
                self.countTimer?.invalidate()
                self.progressView.setProgress(progress: 1.0, animated: false)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    let resultVC = self.storyboard?.instantiateViewController(withIdentifier: "CheckoutResultVC") as! CheckoutResultVC
                    resultVC.resultUrl = url
                    self.navigationController?.pushViewController(resultVC, animated: true)
                }
                
            }
            else{
                if data == nil {
                    self.showToast(Constant.ERROR_CONNECT, duration: 2, position: .bottom)
                } else {
                    let dict = JSON(data as Any)
                    let result = dict[Constant.RESULT_CODE].stringValue
                    let message = dict[Constant.RES_MESSAGE].stringValue
                    if result == "error" {
                        self.showToast("\(message)", duration: 2, position: .bottom)
                    }
                    else {
                        self.showToast("Login Failed.", duration: 2, position: .bottom)
                    }
                }
            }
        })
    }
    
    @objc func onClickEditImage(_ sender: UIButton) {
        
        let editImageVC = self.storyboard?.instantiateViewController(withIdentifier: "EditImageVC") as! EditImageVC
        selectedIndex = sender.tag
        editImageVC.selectedCart = CartList[sender.tag]
        self.navigationController?.pushViewController(editImageVC, animated: true)
    }
}


//MARK:--CollectionView Delegate
extension CheckoutVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
    }
}

//MARK: UICollectionViewDataSource
extension CheckoutVC : UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return CartList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OrderListCell", for: indexPath) as! OrderListCell
        
        cell.entity = CartList[indexPath.row]
        cell.productCountLabel.text = "\(CartList[indexPath.row].product_count)"
        cell.addButton.tag = indexPath.row
        cell.addButton.addTarget(self, action: #selector(onPlus), for: .touchUpInside)
        cell.removeButton.tag = indexPath.row
        cell.removeButton.addTarget(self, action: #selector(onMinus), for: .touchUpInside)
        
        let uploadImageData = JSON(parseJSON: CartList[indexPath.row].productUploadJSONString).arrayValue
        if  uploadImageData.count == 0 {
            cell.editImageButton.isHidden = true
            cell.editImageButtonHeight.constant = 0
            cell.tiTleLabelConstraint.constant = 20
        } else {
            cell.editImageButton.isHidden = false
            cell.editImageButtonHeight.constant = 25
            if CartList[indexPath.row].uploadStatus {
                cell.editImageButton.backgroundColor = UIColor(named: "AssistColor")
                cell.editImageButton.setTitle("READY TO SUBMIT", for: .normal)
                cell.editImageButton.setTitleColor(.white, for: .normal)
            } else {
                cell.editImageButton.backgroundColor = UIColor(named: "TransparentColor")
                cell.editImageButton.setTitle("EDIT IMAGES", for: .normal)
                cell.editImageButton.setTitleColor(UIColor(named: "PrimaryColor"), for: .normal)
            }
        }
        cell.editImageButton.tag = indexPath.row
        cell.editImageButton.addTarget(self, action: #selector(onClickEditImage(_:)), for: .touchUpInside)
        return cell
    }
}

extension CheckoutVC: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        
        let h: CGFloat = 140
        let w = collectionView.frame.size.width
        
        return CGSize(width: w, height: h)
        
    }
    
}

extension CheckoutVC {
    
    func createOrder(cart: [TestCartModel], completion: @escaping(_ success:Bool, _ response: Any?)-> ()){
        
        let token = Defaults[\.device_token]!
        let header: HTTPHeaders = ["Authorization": "\(token)"]
        let URL = AUTH + "checkout"
        var params:[String:String] = [:]
        
        for i in 0 ..< CartList.count {
            
            params["items[\(i)][product_id]"] = "\(CartList[i].product_id)"
            params["items[\(i)][variation_id]"] = "\(CartList[i].variation_id)"
            params["items[\(i)][quantity]"] = "\(CartList[i].product_count)"
            
        }
        
        let upload = session.upload(
            multipartFormData: { (multipartFormData) in
                
                for (key, value) in params {
                    if let data = value.data(using: .utf8) {
                        multipartFormData.append(data, withName: key)
                    }
                }
                var index = 0
                for i in 0 ..< CartList.count {
                    
                    let tempImgs = CartList[i].productImagesData
                    
                    for (key, value) in tempImgs.enumerated() {
                        let data = value.jpegData(compressionQuality: 0.8)
                        multipartFormData.append(data!, withName: "images[\(index)]", fileName: "\(CartList[i].product_id)_\(CartList[i].variation_id)_\(key)" + ".png", mimeType: "image/png")
                        index += 1
                    }
                }
            },
            to: URL,
            usingThreshold: multipartFormDataEncodingMemoryThreshold,
            method: .post,
            headers: header,
            interceptor: nil,
            fileManager: FileManager.default)
            
            .uploadProgress(queue: .main) { (progress) in
                UploadProgress = progress.fractionCompleted
                self.progressView.setProgress(progress: CGFloat(progress.fractionCompleted), animated: true)
        }
        
        upload.responseJSON { (response) in
            
            switch response.result {
                
            case .failure:
                completion(false, nil)
                
            case .success(let data):
                
                completion(true, data)
            }
        }
    }
}
