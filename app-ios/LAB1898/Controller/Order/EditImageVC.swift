//
//  EditImageVC.swift
//  LAB1898
//
//  Created by cs on 2020/4/23.
//  Copyright © 2020 cs. All rights reserved.
//

import UIKit
import ImageScrollView
import SDWebImage
import DKImagePickerController
import DKCamera
import CropViewController
import SwiftyJSON
import MBProgressHUD

class tmpImageStruct {
    var id = 0
    var width:Float = 0
    var height:Float = 0
    var name = ""
    var realWidth = ""
    var realHeight = ""
    var imgFile : UIImage?
}

class EditImageVC: BaseViewController {
    
    @IBOutlet weak var cvPageIndicator: UICollectionView!
    @IBOutlet weak var cvImageUpload: UICollectionView!
    @IBOutlet weak var cvImageUploadFlowLayout: UICollectionViewFlowLayout!
    
    private var indexOfCellBeforeDragging = 0
    var currentIndex = 0
    var currentScale: CGFloat = 1.0
    var currentContentOffset: CGPoint!
    var selectedCart: TestCartModel!
    var imageData = [editImageModel]()
    var selectedImageID = 0
    var selectedImageNo = 0
    
    var imageWidth : Double = 0
    var imageHeight : Double = 0
    var ratio : Double = 0
    
    let deviceHeight = UIScreen.main.bounds.height
    let deviceWidth = UIScreen.main.bounds.width
    let safeAreaHeight = UIApplication.shared.windows.filter {$0.isKeyWindow}.first?.safeAreaInsets.top
    var availableHeight: Float = 0
    
    private var croppingStyle = CropViewCroppingStyle.default
    private var croppedRect = CGRect.zero
    private var croppedAngle = 0
    var imageRect: CGRect!
    
    var getDataFromServer = true
    var dataSource = [tmpImageStruct]()
    
    var hud : MBProgressHUD!

    override func viewDidLoad() {
        super.viewDidLoad()
       
        let backBarButtton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        backBarButtton.tintColor = .white
        navigationItem.backBarButtonItem = backBarButtton
        self.title = "Edit Image"
        cvImageUploadFlowLayout.minimumLineSpacing = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        availableHeight = Float((deviceHeight - safeAreaHeight!)) - 230
        
        loadImageCollectionView()
    }
    
    func loadImageCollectionView() {
        
        if getDataFromServer {
                        
            let dataSourceJson = JSON(parseJSON: selectedCart.productUploadJSONString).arrayValue
            for item in dataSourceJson {
                let one = editImageModel(dict: item)
                   
                if one.repeatCount != 0 {
                    for _ in 0 ..< one.repeatCount {
                        imageData.append(one)
                    }
                } else {
                    imageData.append(one)
                }
            }
            
            getDataFromServer = false
            for (index, item) in imageData.enumerated() {
                let tmp = tmpImageStruct()
                tmp.id = index
                tmp.width = item.width
                tmp.height = item.height
                tmp.name = item.name
                tmp.realWidth = item.real_width
                tmp.realHeight = item.real_height
                
                dataSource.append(tmp)
            }
            
            dataSource.sorted(by: { $0.id < $1.id })
            for item in dataSource {
                print("ID: ", item.id)
            }
            cvImageUpload.reloadData()
            onCheckImageDataStatus()
        } else {
            print("reload data from local")
            cvImageUpload.reloadData()
        }
    }
    
    func onCheckImageDataStatus() {
        
        if !selectedCart.productImagesData.isEmpty {
            let selectedCartImageData = selectedCart.productImagesData
            for (index, item) in selectedCartImageData.enumerated(){
                
                dataSource[index].imgFile = item
            }
//            selectedCart.productImagesData.removeAll()
        } else {
            showAlert(title: "LAB1898", message: "Please select photos to upload", positive: Constant.OK, negative: nil){
                
                let galleryAction = UIAlertAction(title: "From Gallery", style: .destructive) { (action) in
                    self.openSequatialGallery()
                }
                
                let cancelAction = UIAlertAction(title:"Cancel", style: .cancel, handler : nil)
                let alert = UIAlertController(title: "", message: "Select the Place", preferredStyle: .actionSheet)
                alert.addAction(galleryAction)
                alert.addAction(cancelAction)

                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func scrollCurrentIndicator(index: Int) {
      
        cvPageIndicator.scrollToItem(at:IndexPath(item: index, section: 0), at: .centeredHorizontally, animated: false)
    }
    
    func openSequatialGallery() {
        
        let pickerController = DKImagePickerController()
        pickerController.assetType = .allPhotos
        pickerController.allowSwipeToSelect = true
        pickerController.sourceType = .photo
        pickerController.singleSelect = false
        pickerController.maxSelectableCount = dataSource.count
        pickerController.dismissCamera()
        pickerController.showsCancelButton = true
        pickerController.didSelectAssets = {(assets: [DKAsset]) in
            self.onSelectAssets1(assets: assets)
        }
        pickerController.modalPresentationStyle = .fullScreen
        present(pickerController, animated: true, completion : nil)
    }
    
    func onSelectAssets1(assets : [DKAsset]) {
        if assets.count > 0 {
        
            for (index, asset) in assets.enumerated() {
                asset.fetchOriginalImage(options: nil, completeBlock: { image, info in
                    if let img = image {
                        self.dataSource[index].imgFile = img
                    }
                })
           }
            self.cvImageUpload.reloadData()
            self.cvPageIndicator.reloadData()
//            self.scrollCurrentIndicator(index: self.selectedImageID)
       }else {}
    }

    func openGallery() {
        let pickerController = DKImagePickerController()
        pickerController.assetType = .allPhotos
        pickerController.allowSwipeToSelect = true
        pickerController.sourceType = .photo
        pickerController.singleSelect = true
        pickerController.dismissCamera()
        pickerController.showsCancelButton = true
        pickerController.didSelectAssets = {(assets: [DKAsset]) in
            self.onSelectAssets(assets: assets)
        }
        pickerController.modalPresentationStyle = .fullScreen
        present(pickerController, animated: true, completion : nil)
    }
    
    func onSelectAssets(assets : [DKAsset]) {
           
        if assets.count > 0 {
        
            for asset in assets {
                asset.fetchOriginalImage(options: nil, completeBlock: { image, info in
                       
                    if let img = image {
                        for item in self.dataSource {
                            if item.id == self.selectedImageID {
                                item.imgFile = img
                            }
                        }
                       }
                       self.cvImageUpload.reloadData()
                       self.cvPageIndicator.reloadData()
                       self.scrollCurrentIndicator(index: self.selectedImageID)
                   })
               }
           }
           else {}
    }

    func openCamera(){
        
        let camera = DKCamera()
        camera.allowsRotate = true
        camera.showsCameraControls = true
        camera.defaultCaptureDevice = DKCameraDeviceSourceType.rear
        
        camera.didCancel = {self.dismiss(animated: true, completion: nil)}
        camera.didFinishCapturingImage = { (image: UIImage?, metadata: [AnyHashable : Any]?) in
            self.dismiss(animated: true, completion: nil)
            
            if let img = image {
                for item in self.dataSource {
                    if item.id == self.selectedImageID {
                        item.imgFile = img
                    }
                }
                self.scrollCurrentIndicator(index: self.selectedImageID)
            }
        }
        camera.modalPresentationStyle = .fullScreen
        present(camera, animated: true, completion: nil)
    }

    @IBAction func onClickSave(_ sender: Any) {
        selectedCart.productImagesData.removeAll()
        for (index, item) in dataSource.enumerated() {
            if item.imgFile == nil {
                showToast("You didn't add \(index + 1)th Image.")
                return
            }
            
            selectedCart.productImagesData.append(item.imgFile!)
            selectedCart.uploadStatus = true
        }
//        saveCartData()
        CartList[selectedIndex] = selectedCart
        doDismiss()
    }
}


extension EditImageVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == cvPageIndicator {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PageIndicatorCell", for: indexPath) as! PageIndicatorCell
            cell.lblPageNumber.text = "\(indexPath.row + 1)"
            
            if currentIndex < indexPath.row {
                cell.lblPageNumber.backgroundColor = .gray
            } else if currentIndex > indexPath.row {
                cell.lblPageNumber.backgroundColor = UIColor(named: "AssistColor")
            } else {
                cell.lblPageNumber.backgroundColor = UIColor(named: "PrimaryColor")
            }
            
            return cell
            
        } else {
            self.currentScale = 1.0
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageUploadCell", for: indexPath) as! ImageUploadCell
            
            
            cell.photoImageView.image = dataSource[indexPath.row].imgFile
            
            if cell.photoImageView.image != nil {
                  cell.placeholderLabel.isHidden = true
            } else {
                  cell.placeholderLabel.isHidden = false
            }
            
            if dataSource[indexPath.row].realWidth != "" {
                cell.real_widthLabel.isHidden = false
                cell.real_widthLabel.text = dataSource[indexPath.row].realWidth
            } else {
                cell.real_widthLabel.isHidden = true
            }
            
            if dataSource[indexPath.row].realHeight != "" {
                cell.real_heightLabel.text = dataSource[indexPath.row].realHeight
                cell.real_heightLabel.isHidden = false
            } else {
                cell.real_heightLabel.isHidden = true
            }
            
            let tmpRatio: CGFloat  = CGFloat(dataSource[indexPath.row].width / dataSource[indexPath.row].height)
            let tmpWidth: CGFloat  = deviceWidth - 60
            let tmpHeight: CGFloat = tmpWidth / tmpRatio
            
            if tmpHeight > CGFloat(availableHeight) {
                cell.imageWidth.constant = CGFloat(availableHeight) * tmpRatio
                cell.imageHeight.constant = CGFloat(availableHeight)
            } else {
                cell.imageWidth.constant  = tmpWidth
                cell.imageHeight.constant = tmpHeight
            }

            cell.addImageBlock = {
                self.selectedImageID = self.dataSource[indexPath.row].id
                print("You clicked button", self.dataSource[indexPath.row].id)
                self.addImage()
            }
            self.selectedImageNo = indexPath.row
            cell.cropImageBlock = {
                
                if cell.photoImageView.image == nil {
                    self.showToast("You didn't add image.")
                    return
                } else {
                    self.doCropImage(imageView: cell.photoImageView)
                }
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == cvPageIndicator {
            
            return CGSize(width: 40, height: 40)
        } else {
            
            return CGSize(width:collectionView.frame.size.width, height: collectionView.frame.size.height)
        }
    }
    
    fileprivate func addImage() {
            
        let galleryAction = UIAlertAction(title: "From Gallery", style: .destructive) { (action) in
            self.openGallery()
        }
        let cameraAction = UIAlertAction(title: "From Camera", style: .destructive) { (action) in
            self.openCamera()
        }
        let cancelAction = UIAlertAction(title:"Cancel", style: .cancel, handler : nil)
        let alert = UIAlertController(title: "", message: "Select the Place", preferredStyle: .actionSheet)
        alert.addAction(galleryAction)
        alert.addAction(cameraAction)
        alert.addAction(cancelAction)

        self.present(alert, animated: true, completion: nil)
       
    }
   
    fileprivate func doCropImage(imageView: UIImageView) {
        
        let cgRect = CGRect(x: 0, y: 0, width: Int(dataSource[currentIndex].width*1500), height: Int(dataSource[currentIndex].height*1500))
        
        let cropViewController = CropViewController(croppingStyle: self.croppingStyle, image: imageView.image!)
        cropViewController.delegate = self
        cropViewController.imageCropFrame = cgRect
//        cropViewController.aspectRatioPreset = .presetCustom
        cropViewController.aspectRatioLockEnabled = true
        cropViewController.resetAspectRatioEnabled = false
        cropViewController.resetButtonHidden = true
        cropViewController.aspectRatioPickerButtonHidden = true
        
        let viewFrame = view.convert(imageView.frame, to: navigationController!.view)

        cropViewController.presentAnimatedFrom(self,
                                               fromImage: imageView.image,
                                               fromView: nil,
                                               fromFrame: viewFrame,
                                               angle: self.croppedAngle,
                                               toImageFrame: cgRect,
                                               setup: { imageView.isHidden = false },
                                               completion: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
      
        let tempIndex = self.cvImageUpload.contentOffset.x / self.cvImageUpload.frame.size.width
        if ceil(tempIndex) == tempIndex {
            currentIndex = Int(tempIndex)
            cvPageIndicator.reloadData()
            scrollCurrentIndicator(index: currentIndex)
      }
    }
}
extension EditImageVC: CropViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.croppedRect = cropRect
        self.croppedAngle = angle
        
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }

    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {

        dataSource[currentIndex].imgFile = image
        self.navigationItem.leftBarButtonItem?.isEnabled = true
        cropViewController.dismiss(animated: true, completion: nil)
    }
}



