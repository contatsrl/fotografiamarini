//
//  WebViewVC.swift
//  LAB1898
//
//  Created by cs on 2020/4/18.
//  Copyright © 2020 cs. All rights reserved.
//

import UIKit
import WebKit
import SwiftyUserDefaults

var loadingPage = ""
class WebViewVC: BaseViewController, WKNavigationDelegate {
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var indicatorView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        indicatorView.isHidden = false
        loadingIndicator.startAnimating()
        webView.navigationDelegate = self
        loadWebpage()
    }
    
    func loadWebpage() {
        
        if loadingPage == "contact" {
            self.title = "Contact Us"
            let url = URL(string: "http://www.fotografiamarini.com/contattaci/")!
            webView.load(URLRequest(url: url))
        } else if loadingPage == "history" {
            self.title = "Our History"
            let url = URL(string: "http://www.fotografiamarini.com/storia/")!
            webView.load(URLRequest(url: url))
        } else if loadingPage == "website"{
            self.title = "Our Website"
            let url = URL(string: "http://www.fotografiamarini.com/")!
            webView.load(URLRequest(url: url))
        } else {

            self.title = "Order History"
            let urlString = "https://lab1898.socialtab.it/index.php/my-account/orders/?Authorization=" + String(Defaults[\.device_token]!)
            let url = URL(string: urlString)!
            webView.load(URLRequest(url: url))
        }
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        indicatorView.isHidden = false
        loadingIndicator.isHidden = false
        loadingIndicator.startAnimating()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        loadingIndicator.stopAnimating()
        indicatorView.isHidden = true
        loadingIndicator.isHidden = true
    }

    @IBAction func onClickMenu(_ sender: Any) {
        
        panel?.openLeft(animated: true)
    }
}
