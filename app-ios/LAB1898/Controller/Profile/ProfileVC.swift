//
//  ProfileVC.swift
//  LAB1898
//
//  Created by cs on 2020/4/4.
//  Copyright © 2020 cs. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftyJSON
import SwiftyUserDefaults
import DKImagePickerController
import DKCamera
import CropViewController
import SDWebImage
import MBProgressHUD


class ProfileVC: BaseViewController {

    @IBOutlet weak var firstNameField: SkyFloatingLabelTextField!
    @IBOutlet weak var userPhotoImageView: UIImageView!
    @IBOutlet weak var lastNameField: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneNumberField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailField: SkyFloatingLabelTextField!
    @IBOutlet weak var addressField: SkyFloatingLabelTextField!
    
    private var croppingStyle = CropViewCroppingStyle.default
    private var croppedRect = CGRect.zero
    private var croppedAngle = 0
    var firstName = ""
    var lastName = ""
    var photo_url = ""
    var email = ""
    var phoneNumber  = ""
    var hud : MBProgressHUD!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let img = UIImage(named: "mainLogo")
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        imageView.contentMode = .scaleAspectFit
        imageView.image = img
        self.navigationItem.titleView = imageView
        
        initView()
        
    }
    
    func initView() {
        
        self.hud = self.progressHUD(view: self.view, mode: .indeterminate)
        self.hud.show(animated: true)

        ApiManager.getUserData(userId: Defaults[\.userId]!, completion: {(isSuccess,data) in

            
            if isSuccess{

                let dict = JSON(data!)
                let userData = dict[Constant.DATA]
                let user = UserModel(dict: userData)
                
                self.emailField.text = user.email
                self.firstNameField.text = user.firstName
                self.lastNameField.text = user.lastName
                self.phoneNumberField.text = user.phoneNumber
                self.userPhotoImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
                self.userPhotoImageView.sd_setImage(with: URL(string: user.photo_url), placeholderImage: Constant.AVATAR_IMAGE)
                self.hud.hide(animated: true)
               
            }
            else{
               if data == nil {
                   self.showToast(Constant.ERROR_CONNECT, duration: 2, position: .bottom)
               } else {
                   let dict = JSON(data as Any)
                   let result = dict[Constant.RESULT_CODE].stringValue
                   let message = dict[Constant.RES_MESSAGE].stringValue
                   if result == "error" {
                       self.showToast("\(message)", duration: 2, position: .bottom)
                   }
                   else {
                       self.showToast("Something went wrong.", duration: 2, position: .bottom)
                   }
               }
                self.hud.hide(animated: true)
            }
        })
    }

    @IBAction func onClickMenu(_ sender: Any) {
        
        panel?.openLeft(animated: true)
    }
    
    @IBAction func onClickUpdate(_ sender: Any) {
        
        firstName = firstNameField.text!
        lastName = lastNameField.text!
        phoneNumber = phoneNumberField.text!
        email = emailField.text!
        let image = userPhotoImageView.image!
        
        self.hud = self.progressHUD(view: self.view, mode: .indeterminate)
        self.hud.show(animated: true)

        ApiManager.updateprofile(userId: Defaults[\.userId]!, firstName: firstName, lastName: lastName, phoneNumber: phoneNumber, email: email, photo: image, completion: {(isSuccess,data) in

            self.hud.hide(animated: true)
            if isSuccess{

                let dict = JSON(data!)
                let userData = dict[Constant.DATA]
                let user = UserModel(dict: userData)
                
                self.emailField.text = user.email
                self.firstNameField.text = user.firstName
                self.lastNameField.text = user.lastName
                self.phoneNumberField.text = user.phoneNumber
                self.userPhotoImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
                self.userPhotoImageView.sd_setImage(with: URL(string: user.photo_url), placeholderImage: Constant.PLACEHOLDER_IMAGE)
            }
            else{
               if data == nil {
                   self.showToast(Constant.ERROR_CONNECT, duration: 2, position: .bottom)
               } else {
                   let dict = JSON(data as Any)
                   let result = dict[Constant.RESULT_CODE].stringValue
                   let message = dict[Constant.RES_MESSAGE].stringValue
                   if result == "error" {
                       self.showToast("\(message)", duration: 2, position: .bottom)
                   }
                   else {
                       self.showToast("Something went wrong.", duration: 2, position: .bottom)
                   }
               }
            }
        })
    }
    
    
    @IBAction func onProfileClicked(_ sender: Any) {
        let galleryAction = UIAlertAction(title: "From Gallery", style: .destructive) { (action) in
            self.openGallery()
        }
        let cameraAction = UIAlertAction(title: "From Camera", style: .destructive) { (action) in
            self.openCamera()
        }
        let cancelAction = UIAlertAction(title:"Cancel", style: .cancel, handler : nil)

            // Create and configure the alert controller.
        let alert = UIAlertController(title: "", message: "Select the Place", preferredStyle: .actionSheet)
        alert.addAction(galleryAction)
        alert.addAction(cameraAction)
        alert.addAction(cancelAction)

        self.present(alert, animated: true, completion: nil)
    }
    
    func openGallery() {
       self.croppingStyle = .circular
       
       let imagePicker = UIImagePickerController()
       imagePicker.modalPresentationStyle = .popover
       imagePicker.preferredContentSize = CGSize(width: 320, height: 568)
       imagePicker.sourceType = .photoLibrary
       imagePicker.allowsEditing = false
       imagePicker.delegate = self
       self.present(imagePicker, animated: true, completion: nil)
    }
    
    
    
    func openCamera(){
        
        self.croppingStyle = .circular
        
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .camera
        imagePicker.allowsEditing = false
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
    }
    
}

extension ProfileVC: CropViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let image = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage) else { return }
        
        let cropController = CropViewController(croppingStyle: croppingStyle, image: image)
        cropController.delegate = self
        self.userPhotoImageView.image = image
        
        //If profile picture, push onto the same navigation stack
        if croppingStyle == .circular {
            if picker.sourceType == .camera {
                picker.dismiss(animated: true, completion: {
                    self.present(cropController, animated: true, completion: nil)
                })
            } else {
                picker.pushViewController(cropController, animated: true)
            }
        }
        else { //otherwise dismiss, and then present from the main controller
            picker.dismiss(animated: true, completion: {
                self.present(cropController, animated: true, completion: nil)
                //self.navigationController!.pushViewController(cropController, animated: true)
            })
        }
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.croppedRect = cropRect
        self.croppedAngle = angle
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.croppedRect = cropRect
        self.croppedAngle = angle
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
        self.userPhotoImageView.image = image
        layoutImageView()
        
        self.navigationItem.leftBarButtonItem?.isEnabled = true
        
        if cropViewController.croppingStyle != .circular {
            userPhotoImageView.isHidden = true
            
            cropViewController.dismissAnimatedFrom(self, withCroppedImage: image,
                                                   toView: userPhotoImageView,
                                                   toFrame: CGRect.zero,
                                                   setup: { self.layoutImageView() },
                                                   completion: {
                                                    self.userPhotoImageView.isHidden = false })
        }
        else {
            self.userPhotoImageView.isHidden = false
            cropViewController.dismiss(animated: true, completion: nil)
        }
    }
    
    public func layoutImageView() {
        guard userPhotoImageView.image != nil else { return }
        
        let padding: CGFloat = 20.0
        
        var viewFrame = self.view.bounds
        viewFrame.size.width -= (padding * 2.0)
        viewFrame.size.height -= ((padding * 2.0))
        
        var imageFrame = CGRect.zero
        imageFrame.size = userPhotoImageView.image!.size;
        
        if userPhotoImageView.image!.size.width > viewFrame.size.width || userPhotoImageView.image!.size.height > viewFrame.size.height {
            let scale = min(viewFrame.size.width / imageFrame.size.width, viewFrame.size.height / imageFrame.size.height)
            imageFrame.size.width *= scale
            imageFrame.size.height *= scale
            imageFrame.origin.x = (self.view.bounds.size.width - imageFrame.size.width) * 0.5
            imageFrame.origin.y = (self.view.bounds.size.height - imageFrame.size.height) * 0.5
            userPhotoImageView.frame = imageFrame
        }
        else {
            self.userPhotoImageView.frame = imageFrame;
            self.userPhotoImageView.center = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.midY)
        }
    }
}
