//
//  CategoryVC.swift
//  LAB1898
//
//  Created by cs on 2020/4/3.
//  Copyright © 2020 cs. All rights reserved.
//

import UIKit

class ProductListVC: BaseViewController {

    @IBOutlet weak var categoryDescriptionLabel: UILabel!
    @IBOutlet weak var productList: UITableView!
    @IBOutlet weak var cartView: UIView!
    @IBOutlet weak var cartCountLabel: UILabel!
    
    var categoryDescription = ""
    var  productData = [ProductModel]()
    var categoryName = ""
//    var category = [CategoryModel]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let backBarButtton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        backBarButtton.tintColor = .white
        navigationItem.backBarButtonItem = backBarButtton
        
        navigationItem.title = categoryName
//        loadCartData()
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if CartList.count == 0 {
            cartCountLabel.isHidden = true
        } else {
            cartCountLabel.text = "\(CartList.count)"
            cartCountLabel.isHidden = false
        }
    }
    
    func initView() {
        categoryDescriptionLabel.text = categoryDescription
    }
    @IBAction func onClickCart(_ sender: Any) {
        if CartList.isEmpty {
            showToast("You didn't select anything.")
            return
        }
        gotoNavVC("CheckoutVC")
        print("test string=======", String(format:"%.2f", 10.0))
    }
    
}
//MARK:-- TableViewDelegate
extension ProductListVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let productDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "ProductVC") as! ProductVC
        productDetailVC.product = productData[indexPath.row]
      
        self.navigationController?.pushViewController(productDetailVC, animated: true)
    }
}

//MARK: TableViewDataSource
extension ProductListVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! CategoryCell
        cell.entity = productData[indexPath.row]
        return cell
    }
}

