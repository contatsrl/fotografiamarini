//
//  CategoryListVC.swift
//  LAB1898
//
//  Created by cs on 2020/4/3.
//  Copyright © 2020 cs. All rights reserved.
//

import UIKit
import FAPanels
import SwiftyJSON
import MBProgressHUD


class CategoryListVC: BaseViewController {

    var per_page = 100
    var page = 1
    
    @IBOutlet weak var categoryList: UITableView!
    
    var menuVC : MenuVC!
    var hud : MBProgressHUD!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let backBarButtton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        backBarButtton.tintColor = .white
        navigationItem.backBarButtonItem = backBarButtton
        
        let img = UIImage(named: "mainLogo")
        let imageView = UIImageView(frame: CGRect(x: 0, y: -10, width: 30, height: 30))
        imageView.contentMode = .scaleAspectFit
        imageView.image = img
        self.navigationItem.titleView = imageView
        
        
        categoryList.tableFooterView = UIView()
        viewConfigurations()        
        
        
        getAllCategory()
        getAllProducts()
    }
    
    func getAllCategory() {
        
        CategoryList.removeAll()
        
        ApiManager.getAllCategory { (isSuccess, data) in
            
            if isSuccess {
                
                let dicts = JSON(data!)
                print("Categories", dicts)
                let dictArr = dicts.arrayValue
                for dict in dictArr {
                    let category = CategoryModel(dict: dict)
                    CategoryList.append(category)
                }
                self.categoryList.reloadData()
            } else {
                self.showToast("Something went worng\nTry again later")
            }
        }
    }
    
    func getAllProducts() {
        if page == 1 {
            ProductList.removeAll()
        }
        self.hud = self.progressHUD(view: self.view, mode: .indeterminate)
        self.hud.show(animated: true)
        
        ApiManager.getAllProducts(per_page, page: page) { (isSuccess, data) in
            
            self.hud.hide(animated: true)
            if isSuccess {
                
                let dicts = JSON(data!)
                print("Products", dicts)
                let dictArr = dicts.arrayValue
                for dict in dictArr {
                    let product = ProductModel(dict: dict)
                    ProductList.append(product)
                    
                }
                
                if dictArr.count == self.per_page {
                    self.page += 1
                    self.getAllProducts()
                } else{}
            } else {
                self.showToast("Something went worng\nTry again later")
            }
        }
    }
    
    func viewConfigurations() {

            //  Resetting the Panel Configs...
            
            panel!.configs = FAPanelConfigurations()
            panel?.leftPanelPosition = .front //.back
            panel!.configs.panFromEdge = false
            panel!.configs.minEdgeForLeftPanel = 40 //CGFloat(Double(sliderValueAsText)!)
            panel!.configs.centerPanelTransitionDuration = 1 //TimeInterval(Double(valueAsText)!)
    //        panel!.configs.rightPanelWidth = 50
            panel!.configs.leftPanelWidth = 0
            panel!.configs.canLeftSwipe = true
            
            let animOptions: FAPanelTransitionType = .moveLeft
    //        .flipFromLeft, .flipFromRight, .flipFromTop, .flipFromBottom, .curlUp, .curlDown, .crossDissolve
    //        .moveRight, .moveLeft, .moveUp, .moveDown, .splitHorizontally, .splitVertically, .dumpFall, .boxFade,
            panel!.configs.centerPanelTransitionType = animOptions
            panel!.configs.bounceOnRightPanelOpen = false
            panel!.configs.bounceDuration = 0.2
            
            panel!.delegate = self
        }
    
    @IBAction func onClickMenu(_ sender: Any) {
        
        panel?.openLeft(animated: true)
    }
}
//MARK:-- TableViewExtension
extension CategoryListVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var productData = [ProductModel]()
        let category = CategoryList[indexPath.row]
        for product in ProductList {
            if product.categoryIds.contains(category.categoryId) {
                productData.append(product)
            }
        }
        
        
        let productListVC = self.storyboard?.instantiateViewController(withIdentifier: "ProductListVC") as! ProductListVC
        productListVC.productData = productData
        productListVC.categoryDescription = category.categoryDescription
        productListVC.categoryName = category.categoryName
        self.navigationController?.pushViewController(productListVC, animated: true)
    }
}

//MARK: TableViewDataSource
extension CategoryListVC : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CategoryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryListCell", for: indexPath) as! CategoryListCell
        cell.entity = CategoryList[indexPath.row]
        return cell
    }
}

//MARK:--FAPanels Extension (SideMenu)
extension CategoryListVC: FAPanelStateDelegate {
    
    func centerPanelWillBecomeActive() {
        print("centerPanelWillBecomeActive called")
    }
    
    func centerPanelDidBecomeActive() {
        print("centerPanelDidBecomeActive called")
    }
    
    
    func leftPanelWillBecomeActive() {
        print("leftPanelWillBecomeActive called")
    }
    
    
    func leftPanelDidBecomeActive() {
        print("leftPanelDidBecomeActive called")
    }
    
    
    func rightPanelWillBecomeActive() {
        print("rightPanelWillBecomeActive called")
    }
    
    func rightPanelDidBecomeActive() {
        print("rightPanelDidBecomeActive called")
    }
    
}

