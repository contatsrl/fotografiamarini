//
//  ProductVC.swift
//  LAB1898
//
//  Created by cs on 2020/4/3.
//  Copyright © 2020 cs. All rights reserved.
//

import UIKit
import iOSDropDown
import AlamofireImage
import SwiftSoup
import SwiftyUserDefaults
import SwiftyJSON
import MBProgressHUD

var attributeValues = [String]()
var product_dimension = ""

class ProductVC: BaseViewController {
    
    open override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }

    @IBOutlet weak var slideShowImageView: ImageSlideshow!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var moreInfoLabel: UILabel!
    @IBOutlet weak var lblCartCount: UILabel!
    @IBOutlet weak var attributeList: DynamicHeightCollectionView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var topCommentViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var topCommentView: UIView!
    
    
    var product: ProductModel?
    var variationData = [VariationModel]()
    var alamofireSources = [AlamofireSource]()
    var selectedVariation = 0
    var initImagesData = [UIImage]()
    var attributesData = [AttributeModel]()
    
    var per_page = 100
    var page = 1
    var hud : MBProgressHUD!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let backBarButtton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        backBarButtton.tintColor = .white
        navigationItem.backBarButtonItem = backBarButtton
        self.title = product?.productName
        getVariations()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
   
        attributeValues.removeAll()
        attributesData.removeAll()
        
        initView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        attributeValues.removeAll()
        attributesData.removeAll()
        
    }
    
    func getVariations() {
        
        if page == 1 {
            variationData.removeAll()
        }
        
        self.hud = self.progressHUD(view: self.view, mode: .indeterminate)
        self.hud.show(animated: true)
        
        ApiManager.getVariation(product_id: product!.id, per_page: per_page, page: page, completion: { (isSuccess, data) in
            
            if isSuccess {
                let dict = JSON(data!).arrayValue
                
                for item in dict {
                    
                    let variation = VariationModel(dict: item)
                    self.variationData.append(variation)
                }
                
                if dict.count == self.per_page {
                    self.page += 1
                    self.getVariations()
                } else {
                    self.hud.hide(animated: true)
                }
                
            } else {
                self.showToast("Something went worng\nTry again later")
            }
        })
    }
    
    func initView() {
        
        setSlideShow()
        attributesData = product!.attributesData
        attributeList.reloadData()
        productNameLabel.text = product!.productName
        productPriceLabel.text = "da \(String(format: "%.2f", product!.productPrice))€"
        
        if product!.topComment.isEmpty {
            topCommentView.isHidden = true
            topCommentViewConstraint.constant = 1
        } else {
            topLabel.text = product!.topComment
            topCommentView.isHidden = false
        }
                
        do {
            let doc1: Document = try SwiftSoup.parse(product!.topDescription)
            let doc2: Document = try SwiftSoup.parse(product!.bottomDescritpion)
            self.descriptionLabel.text = try! doc1.text()
            self.moreInfoLabel.text = try! doc2.text()
            
        } catch Exception.Error(let type, let message) {
            print(type)
            print(message)
        } catch {
            print("error")
        }
        
//        loadCartData()
        if CartList.count > 0 {
            lblCartCount.text = "\(CartList.count)"
        } else {
            lblCartCount.isHidden = true
        }
    }
    
    func setSlideShow() {
        
        slideShowImageView.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
        slideShowImageView.contentScaleMode = UIViewContentMode.scaleAspectFill
        
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        pageControl.pageIndicatorTintColor = UIColor.black
        slideShowImageView.pageIndicator = pageControl
        
        
        slideShowImageView.activityIndicator = DefaultActivityIndicator()
        slideShowImageView.delegate = self
        
        alamofireSources.removeAll()
        
        for item in product!.productImages {
            let tmp = AlamofireSource(urlString: item, placeholder: Constant.PLACEHOLDER_IMAGE)!
            alamofireSources.append(tmp)
        }
        
        slideShowImageView.setImageInputs(alamofireSources)

        let recognizer = UITapGestureRecognizer(target: self, action: #selector(ProductVC.didTap))
        slideShowImageView.addGestureRecognizer(recognizer)
    }
    
    @objc func didTap() {
        let fullScreenController = slideShowImageView.presentFullScreenController(from: self)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
    
    @IBAction func onClickBuy(_ sender: Any) {
       
        self.hud = self.progressHUD(view: self.view, mode: .indeterminate)
        self.hud.label.text = "Processing..."
        self.hud.show(animated: true)
        
        if checkValid() {
           
            let variationId = self.getVariationId()
            let  currentTimeStamp = Int(Date().timeIntervalSince1970)
            let cart = TestCartModel(cart_id: currentTimeStamp, product_id: self.product!.id, variation_id: variationId, product_price: self.product!.productPrice, product_count: 1, productImages: self.product!.productImages
                , productImagesData: self.initImagesData, productDimension: product_dimension, productUploadJSONString: self.product!.image_uploadJSON, product_name: self.product!.productName, uploadStatus: false)

            CartList.append(cart)
            self.hud.hide(animated: true)
            DispatchQueue.main.async {
            // Save to local userdefault
//                self.saveCartData()

                self.lblCartCount.isHidden = false
                self.lblCartCount.text = "\(CartList.count)"
                self.lblCartCount.layoutIfNeeded()

                self.hud.hide(animated: true)
            }
        } else {
            hud.hide(animated: true)
        }
    }
    
    fileprivate func checkValid() -> Bool {
        
        for val in attributeValues {
            if val == "0" {
                showToast("You must fill all flelds.")
                return false
            }
        }
        
        if getVariationId() == 0 {
            showToast("Invalid variation.")
            return false
        }
        
        return true
    }
    
    fileprivate func doBuy() {
        
    }
    
    @IBAction func onClickCart(_ sender: Any) {
        
        if CartList.isEmpty {
            showToast("You didn't select anything.")
            return
        }
        gotoNavVC("CheckoutVC")
    }
    
    fileprivate func getVariationId() -> Int {
        var result = 0
        for item in variationData {
                
            if item.variationOption == attributeValues {
                result = item.id
                break
            } else {
                result = 0
            }
        }
        return result
    }
}
//MARK:--ImageSlideShowExtension
extension ProductVC: ImageSlideshowDelegate {
    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
    }
}

//MARK:--CollectionView Delegate
extension ProductVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
    }
}

//MARK: UICollectionViewDataSource
extension ProductVC : UICollectionViewDataSource{
      
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return attributesData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttributeCell", for: indexPath) as! AttributeCell
        cell.entity = attributesData[indexPath.row]
        cell.attributeSelectField.tag = indexPath.row
        attributeValues.append("0")
        return cell
        
    }
}

extension ProductVC: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let h: CGFloat = 60
        let w = collectionView.frame.size.width
        
        return CGSize(width: w, height: h)
        
    }
    
}

