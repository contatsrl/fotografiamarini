//
//  UILabel+Extension.swift
//  LAB1898
//
//  Created by cs on 2020/4/22.
//  Copyright © 2020 cs. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    @IBInspectable
    var rotation: Int {
        get {
            return 0
        } set {
            let radians = CGFloat(CGFloat(Double.pi) * CGFloat(newValue) / CGFloat(180.0))
            self.transform = CGAffineTransform(rotationAngle: radians)
        }
    }
}
