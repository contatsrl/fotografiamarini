//
//  AttrbuteModel.swift
//  LAB1898
//
//  Created by cs on 2020/4/21.
//  Copyright © 2020 cs. All rights reserved.
//

import Foundation
import SwiftyJSON

class AttributeModel {
    
    var name = ""
    var options = [String]()
    
    init(dict: JSON) {
        self.name = dict["name"].stringValue
        let optionsData = dict["options"].arrayValue
        for option in optionsData {
            options.append(option.stringValue)
        }
    }
}
