//
//  editImageModel.swift
//  LAB1898
//
//  Created by cs on 2020/4/22.
//  Copyright © 2020 cs. All rights reserved.
//

import Foundation
import SwiftyJSON

class editImageModel {
   
    var id = 0
    var height : Float = 0
    var width : Float = 0
    var name = ""
    var real_height = ""
    var real_width = ""
    var repeatCount : Int = 0
    var imgFile : UIImage?
    
    init(dict : JSON) {

        self.height = dict["height"].floatValue
        self.width = dict["width"].floatValue
        self.name = dict["name"].stringValue
        self.real_width = dict["real_width"].stringValue
        self.real_height = dict["real_height"].stringValue
        self.repeatCount = dict["repeat"].intValue
    }
}

