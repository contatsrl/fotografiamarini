//
//  UserModel.swift
//  LAB1898
//
//  Created by cs on 2020/4/11.
//  Copyright © 2020 cs. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserModel {
    
    var firstName = ""
    var lastName = ""
    var email = ""
    var phoneNumber = ""
    var address = ""
    var photo_url = ""
    
    init(dict: JSON) {
        
        self.email = dict["data"]["user_email"].stringValue
        if dict["meta"]["avatar"].exists() {
            self.photo_url = dict["meta"]["avatar"].arrayValue[0].stringValue
        }
        if dict["meta"]["phone"].exists() {
            self.phoneNumber = dict["meta"]["phone"].arrayValue[0].stringValue
        }
        self.firstName = dict["meta"]["first_name"].arrayValue[0].stringValue
        self.lastName = dict["meta"]["last_name"].arrayValue[0].stringValue
    }
}
