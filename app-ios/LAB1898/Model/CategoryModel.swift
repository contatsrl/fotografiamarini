//
//  CategoryListModel.swift
//  LAB1898
//
//  Created by cs on 2020/4/3.
//  Copyright © 2020 cs. All rights reserved.
//

import Foundation
import SwiftyJSON

class CategoryModel {
    
    var categoryId = 0
    var categoryImage = ""
    var categoryName = ""
    var categoryStartPrice = ""
    var categoryDescription = ""
    
    
    init(categoryImage: String, categoryName: String, categoryStartPrice: String) {
        self.categoryImage = categoryImage
        self.categoryName = categoryName
        self.categoryStartPrice = categoryStartPrice
    }
    
    init(dict: JSON) {
        
        self.categoryId = dict["id"].intValue
        self.categoryImage = dict["image"]["src"].stringValue
        //self.categoryName = dict["name"].stringValue
        let name = dict["name"].stringValue
        
        self.categoryName = String(htmlEncodedString: name)!
        self.categoryDescription = dict["description"].stringValue
    }
}
