//
//  VariationModel.swift
//  LAB1898
//
//  Created by cs on 2020/4/18.
//  Copyright © 2020 cs. All rights reserved.
//

import Foundation
import SwiftyJSON

class VariationModel {
    
    var id = 0
    var dimension = ""
    var paperType = ""
    var variationOption = [String]()
    
    init(dict: JSON) {
        self.id = dict["id"].intValue
        let attributes = dict["attributes"].arrayValue
        for item in attributes {
//            let name = item["name"].stringValue
            let option = item["option"].stringValue
            self.variationOption.append(option)
        }
    }
}
