//
//  CategoryModel.swift
//  LAB1898
//
//  Created by cs on 2020/4/3.
//  Copyright © 2020 cs. All rights reserved.
//

import Foundation
import SwiftyJSON
import SwiftSoup

class ProductModel {
    
    var id = 0
    var productImages = [String]()
    var productName = ""
    var productPrice : Float = 0
    var productCartCount = 0
    var imageCountString = ""
    var categoryIds = [Int]()
    var topDescription = ""
    var bottomDescritpion = ""
    var variation_id = [Int]()
    var attributesData = [AttributeModel]()
    var image_uploadList = [editImageModel]()
    var image_uploadJSON = ""
    var topComment = ""
    
    init(productImage: [String], productName: String, productPrice: Float, productCartCount: Int, imageCountString: String, variation_id: [Int] ) {
        
        self.productImages = productImage
        self.productName = productName
        self.productPrice = productPrice
        self.productCartCount = productCartCount
        self.imageCountString = imageCountString
        self.variation_id = variation_id
        
    }
    
    init(dict: JSON) {
        
        self.id = dict["id"].intValue
        let dictImages = dict["images"].arrayValue
        for dictImage in dictImages {
            let imageUrl = dictImage["src"].stringValue
            self.productImages.append(imageUrl)
        }
        self.productName = dict["name"].stringValue
        self.productPrice = dict["price"].floatValue
        self.productCartCount = dict["total_sales"].intValue
        
        let categories = dict["categories"].arrayValue
        for cat in categories {
            let catid = cat["id"].intValue
            self.categoryIds.append(catid)
        }
        let meta_data = dict["meta_data"].arrayValue
        for item in meta_data {
            let data_key = item["key"].stringValue
            if data_key == "image_overlay_text" {
                self.imageCountString = item["value"].stringValue
            }else if data_key == "_upload_list" {
                self.image_uploadJSON = item["value"].rawString()!
                /*
                let uploadData = item["value"].arrayValue
                for item in uploadData {
                    let one  = editImageModel(dict: item)
                    if one.repeatCount != 0 {
                        for _ in 0 ..< one.repeatCount {
                            self.image_uploadList.append(one)
                        }
                    } else {
                        self.image_uploadList.append(one)
                    }
                }
                 */
            } else {
                self.topComment = item["value"].stringValue
            }
        }
        
        let top_description = dict["short_description"].stringValue
        let bottom_description = dict["description"].stringValue
        self.topDescription = top_description
        self.bottomDescritpion = bottom_description
        
        let attributesArray = dict["attributes"].arrayValue
        for item in attributesArray {
            
            let one = AttributeModel(dict : item)
            self.attributesData.append(one)
//            let name = item["name"].stringValue
//            let options = item["options"].arrayValue
//
//            for option in options {
//                if name == "Dimensioni" {
//                    self.dimension.append(option.stringValue)
//                } else {
//                    self.paperType.append(option.stringValue)
//                }
//            }
        }
        let variations = dict["variations"].arrayValue
        for one in variations {
            self.variation_id.append(one.intValue)
        }
    }
}
