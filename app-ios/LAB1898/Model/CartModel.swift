//
//  CartModel.swift
//  LAB1898
//
//  Created by cs on 2020/4/12.
//  Copyright © 2020 cs. All rights reserved.
//

import UIKit
import SwiftyJSON

class CartModel: NSObject, NSCoding {
    
    var product_id = 0
    var product_count = 0
    var variation_id = 0
    var product_price : Float = 0
    var productImages = [String]()
    var productImagesData = [UIImage]()
    var productDimension = ""
    var productUploadJSONString: String = ""
    var product_name = ""
    var uploadStatus = false
    
    init(product_id: Int, variation_id: Int, product_price: Float, product_count: Int, productImages: [String], productImagesData: [UIImage], productDimension: String, productUploadJSONString: String, product_name: String, uploadStatus: Bool) {
        
        self.product_id = product_id
        self.variation_id = variation_id
        self.product_price = product_price
        self.product_count = product_count
        self.productImages = productImages
        self.productImagesData = productImagesData
        self.productDimension = productDimension
        self.productUploadJSONString = productUploadJSONString
        self.product_name = product_name
        self.uploadStatus = uploadStatus
    }
    
    required convenience init(coder aDecoder: NSCoder) {

        let product_id = aDecoder.decodeInteger(forKey: "product_id")
        let product_count = aDecoder.decodeInteger(forKey: "product_count")
        let variation_id = aDecoder.decodeInteger(forKey: "variation_id")
        let product_price = aDecoder.decodeFloat(forKey: "product_price")
        let product_images = aDecoder.decodeObject(forKey: "productImages") as! [String]
        let productDimension = aDecoder.decodeObject(forKey: "productDimension") as! String
        let product_imagesData = aDecoder.decodeObject(forKey: "productImagesData") as! [UIImage]
        let productUploadJSONString = aDecoder.decodeObject(forKey: "productUploadJSONString") as! String
        let product_name = aDecoder.decodeObject(forKey: "product_name") as! String
        let uploadStatus =  aDecoder.decodeBool(forKey: "uploadStatus")
        
        self.init(product_id: product_id, variation_id: variation_id, product_price: product_price, product_count: product_count, productImages: product_images, productImagesData: product_imagesData, productDimension: productDimension, productUploadJSONString: productUploadJSONString, product_name: product_name, uploadStatus: uploadStatus)
    }
    
    func encode(with aCoder: NSCoder) {        
 
        aCoder.encode(product_id, forKey: "product_id")
        aCoder.encode(product_count, forKey: "product_count")
        aCoder.encode(product_price, forKey: "product_price")
        aCoder.encode(productImages, forKey: "productImages")
        aCoder.encode(productImagesData, forKey: "productImagesData")
        aCoder.encode(variation_id, forKey: "variation_id")
        aCoder.encode(productDimension, forKey: "productDimension")
        aCoder.encode(productUploadJSONString, forKey: "productUploadJSONString")
        aCoder.encode(product_name, forKey:  "product_name")
        aCoder.encode(uploadStatus, forKey: "uploadStatus")
    }
}
