//
//  OrderProductModel.swift
//  LAB1898
//
//  Created by cs on 2020/4/6.
//  Copyright © 2020 cs. All rights reserved.
//

import Foundation

class OrderProductModel {
    
    var productName = ""
    var productOrderCount = 0
    var productPrice : Float = 0
    
    init(productName: String, productPrice: Float, productOrderCount: Int) {
        
        self.productName = productName
        self.productPrice = productPrice
        self.productOrderCount = productOrderCount
    }
}
