//
//  OrderInfoModel.swift
//  LAB1898
//
//  Created by cs on 2020/4/11.
//  Copyright © 2020 cs. All rights reserved.
//

import Foundation
import SwiftyJSON
import SwiftyUserDefaults

class OrderInfoModel {
    
    var orderId = 0
    var customerId = 0
    var status = ""
    var customerName = ""
    var customerFamilyName = ""
    var customerEmail = ""
    var customerPhone = ""
    var shippingMethodName = ""
    var shippingAddress = ""
    var shippingName = ""
    var paymentType = ""
    var sendDate = ""
    var acceptedDate = ""
    var orderedProduct = [OrderProductModel]()
    
    init(dict: JSON) {
        
        self.orderId = dict["id"].intValue
        self.customerId = dict["customer_id"].intValue
        
        if customerId == Defaults[\.userId] {
            self.status = dict["status"].stringValue
            self.customerName = dict["billing"]["first_name"].stringValue
            self.customerFamilyName = dict["billing"]["last_name"].stringValue
            self.customerEmail = dict["billing"]["email"].stringValue
            self.customerPhone = dict["billing"]["phone"].stringValue
            
        //        self.shippingMethodName = dict[]
            self.shippingName = dict["shipping"]["first_name"].stringValue + dict["shipping"]["last_name"].stringValue
            self.shippingAddress = dict["shipping"]["address_1"].stringValue + " " + dict["shipping"]["city"].stringValue
            self.paymentType = dict["payment_method"].stringValue
            self.sendDate = dict["date_paid_gmt"].stringValue
            self.acceptedDate = dict["date_completed_gmt"].stringValue
            
            let orderProducts = dict["line_items"].arrayValue
             
            for item in orderProducts {
                    
                let productName = item["name"].stringValue
                let productPrice = item["price"].floatValue
                let productOrderCount = item["quantity"].intValue
                
                self.orderedProduct.append(OrderProductModel(productName: productName, productPrice: productPrice, productOrderCount: productOrderCount))
                
            }
        }
        
    }
    
    
}
