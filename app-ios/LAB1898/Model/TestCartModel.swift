//
//  TestCartModel.swift
//  LAB1898
//
//  Created by cs on 2020/4/26.
//  Copyright © 2020 cs. All rights reserved.
//

import Foundation
import SwiftyJSON

class TestCartModel {
    
    var cart_id = 0
    var product_id = 0
    var product_count = 0
    var variation_id = 0
    var product_price : Float = 0
    var productImages = [String]()
    var productImagesData = [UIImage]()
    var productDimension = ""
    var productUploadJSONString: String = ""
    var product_name = ""
    var uploadStatus = false
    
    init(cart_id: Int, product_id: Int, variation_id: Int, product_price: Float, product_count: Int, productImages: [String], productImagesData: [UIImage], productDimension: String, productUploadJSONString: String, product_name: String, uploadStatus: Bool) {
        
        self.cart_id = cart_id
        self.product_id = product_id
        self.variation_id = variation_id
        self.product_price = product_price
        self.product_count = product_count
        self.productImages = productImages
        self.productImagesData = productImagesData
        self.productDimension = productDimension
        self.productUploadJSONString = productUploadJSONString
        self.product_name = product_name
        self.uploadStatus = uploadStatus
    }
}
