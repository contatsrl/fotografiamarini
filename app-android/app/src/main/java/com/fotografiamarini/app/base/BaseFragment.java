package com.fotografiamarini.app.base;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.fotografiamarini.app.App;

public abstract class BaseFragment extends Fragment {

    private AddFragmentHandler fragmentHandler;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        fragmentHandler = new AddFragmentHandler(getActivity().getSupportFragmentManager());
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getTitle() != null) {
            App.hideLogo();
        } else {
            App.showLogo();
        }
        getActivity().setTitle(getTitle());
    }

    protected abstract String getTitle();

    protected void add(BaseFragment fragment) {
        fragmentHandler.add(fragment);
    }
}
