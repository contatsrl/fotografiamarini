package com.fotografiamarini.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.fotografiamarini.app.R;
import com.fotografiamarini.app.fragment.CategoryFragment;
import com.fotografiamarini.app.model.CategoryModel;

import java.util.ArrayList;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    private ArrayList<CategoryModel> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    public CategoryAdapter(Context context, ArrayList<CategoryModel> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.category_adapter_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CategoryModel category = getItem(position);

        holder.categoryName.setText(category.getName());
        holder.categoryPrice.setText(category.getPrice());
        holder.background.setBackgroundResource(category.getImageUrl());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        LinearLayout background;
        TextView categoryName, categoryPrice;

        ViewHolder(View itemView) {
            super(itemView);
            background = itemView.findViewById(R.id.background);
            categoryName = itemView.findViewById(R.id.categoryName);
            categoryPrice = itemView.findViewById(R.id.categoryPrice);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    CategoryModel getItem(int id) {
        return mData.get(id);
    }

    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
