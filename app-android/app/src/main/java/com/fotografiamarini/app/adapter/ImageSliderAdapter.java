package com.fotografiamarini.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.fotografiamarini.app.R;
import com.fotografiamarini.app.model.ProductModel;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.ArrayList;
import java.util.List;

public class ImageSliderAdapter extends SliderViewAdapter<ImageSliderAdapter.ImageSliderAdapterVH> {

    private Context context;
    private List<ProductModel> products = new ArrayList<>();

    public ImageSliderAdapter(Context context) {
        this.context = context;
    }

    public void renewItems(List<ProductModel> sliderItems) {
        this.products = sliderItems;
        notifyDataSetChanged();
    }

    public void deleteItem(int position) {
        this.products.remove(position);
        notifyDataSetChanged();
    }

    public void addItem(ProductModel product) {
        this.products.add(product);
        notifyDataSetChanged();
    }

    @Override
    public ImageSliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_slider_adapter_item, null);
        return new ImageSliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(ImageSliderAdapterVH viewHolder, final int position) {

        ProductModel product = products.get(position);

        viewHolder.imageViewBackground.setImageResource(product.getImageUrl());
//        Glide.with(viewHolder.itemView)
//                .load(product.getImageUrl())
//                .fitCenter()
//                .into(viewHolder.imageViewBackground);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "This is item in position " + position, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getCount() {
        return products.size();
    }

    class ImageSliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;
        ImageView imageViewBackground;
        ImageView imageGifContainer;

        public ImageSliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);
            imageGifContainer = itemView.findViewById(R.id.iv_gif_container);
            this.itemView = itemView;
        }
    }

}