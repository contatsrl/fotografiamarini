package com.fotografiamarini.app;

import android.app.Application;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.fotografiamarini.app.activity.MainActivity;

public class App extends Application {
    public static final String TAG = App.class.getSimpleName();

    private static MainActivity _mainActivity;
    private static ImageView _logo;
    private static App _instance;

    @Override

    public void onCreate() {
        super.onCreate();
        _instance = this;
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
//        MultiDex.install(this);
    }

    public static synchronized App getInstance() {

        return _instance;
    }

    public static void setMainActivity(MainActivity mainActivity) {
        _mainActivity = mainActivity;
    }

    public static MainActivity getMainActivity() {

        return _mainActivity;
    }

    public static void hideLogo() {
        _logo.setVisibility(View.INVISIBLE);
    }

    public static void showLogo() {
        _logo.setVisibility(View.VISIBLE);
    }

    public static void setLogo(ImageView _logo) {
        App._logo = _logo;
    }

    public static Context getAppContext() {
        return _instance.getApplicationContext();
    }
}
