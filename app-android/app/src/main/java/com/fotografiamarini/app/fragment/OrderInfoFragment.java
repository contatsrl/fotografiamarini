package com.fotografiamarini.app.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fotografiamarini.app.R;
import com.fotografiamarini.app.base.BaseFragment;

public class OrderInfoFragment extends BaseFragment {

    public static OrderInfoFragment newInstance() {
        return new OrderInfoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected String getTitle() {
        return "Order Info";
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_order_info, container, false);

        return view;
    }
}

