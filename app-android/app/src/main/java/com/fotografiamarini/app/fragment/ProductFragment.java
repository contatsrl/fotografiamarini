package com.fotografiamarini.app.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fotografiamarini.app.R;
import com.fotografiamarini.app.listener.RecyclerItemClickListener;
import com.fotografiamarini.app.adapter.ProductAdapter;
import com.fotografiamarini.app.base.BaseFragment;
import com.fotografiamarini.app.model.ProductModel;

import java.util.ArrayList;

public class ProductFragment extends BaseFragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    public static ProductFragment newInstance(String param1, String param2) {
        ProductFragment fragment = new ProductFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    protected String getTitle() {
        return "Category Name";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product, container, false);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                add(DetailFragment.newInstance("Product Name", "Product ID"));
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));

        ArrayList<ProductModel> dataList = new ArrayList<>();

        ProductModel productModel1 = new ProductModel();
        productModel1.setId(0);
        productModel1.setName("PRODUCT NAME");
        productModel1.setImageUrl(R.drawable.category1);
        productModel1.setPrice("from 9.90€");
        dataList.add(productModel1);

        ProductModel productModel2 = new ProductModel();
        productModel2.setId(1);
        productModel2.setName("PRODUCT NAME");
        productModel2.setImageUrl(R.drawable.category2);
        productModel2.setPrice("from 19.90€");
        dataList.add(productModel2);

        ProductModel productModel3 = new ProductModel();
        productModel3.setId(2);
        productModel3.setName("PRODUCT NAME");
        productModel3.setImageUrl(R.drawable.category3);
        productModel3.setPrice("from 10.90€");
        dataList.add(productModel3);

        ProductAdapter productAdapter = new ProductAdapter(getContext(), dataList);
        recyclerView.setAdapter(productAdapter);

        ImageButton checkout = (ImageButton) view.findViewById(R.id.checkout_float);
        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add(CheckoutFragment.newInstance());
            }
        });

        return view;
    }
}
