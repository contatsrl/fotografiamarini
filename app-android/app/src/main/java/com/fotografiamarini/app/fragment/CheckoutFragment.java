package com.fotografiamarini.app.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fotografiamarini.app.R;
import com.fotografiamarini.app.adapter.OrderAdapter;
import com.fotografiamarini.app.base.BaseFragment;
import com.fotografiamarini.app.model.OrderModel;

import java.util.ArrayList;

public class CheckoutFragment extends BaseFragment {

    public static CheckoutFragment newInstance() {
        CheckoutFragment fragment = new CheckoutFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    protected String getTitle() {
        return "Checkout";
    }

    public void openImageUploader() {
        add(ImageUploaderFragment.newInstance());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_checkout, container, false);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(linearLayoutManager);

        ArrayList<OrderModel> dataList = new ArrayList<>();

        OrderModel orderModel1 = new OrderModel();
        orderModel1.setId(0);
        orderModel1.setPrice(7.00f);
        orderModel1.setDescription("This is first product description");
        orderModel1.setOrder_count(2);
        dataList.add(orderModel1);

        OrderModel orderModel2 = new OrderModel();
        orderModel2.setId(1);
        orderModel2.setPrice(15.00f);
        orderModel2.setDescription("This is second product description");
        orderModel2.setOrder_count(1);
        dataList.add(orderModel2);

        OrderAdapter orderAdapter = new OrderAdapter(this, dataList);
        recyclerView.setAdapter(orderAdapter);

        Button order = (Button) view.findViewById(R.id.order);
        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add(OrderInfoFragment.newInstance());
            }
        });

        return view;
    }
}
