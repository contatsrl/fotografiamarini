package com.fotografiamarini.app.fragment;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fotografiamarini.app.R;
import com.fotografiamarini.app.listener.RecyclerItemClickListener;
import com.fotografiamarini.app.adapter.CategoryAdapter;
import com.fotografiamarini.app.base.BackButtonSupportFragment;
import com.fotografiamarini.app.base.BaseFragment;
import com.fotografiamarini.app.model.CategoryModel;

import java.util.ArrayList;

public class CategoryFragment extends BaseFragment implements BackButtonSupportFragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private boolean consumingBackPress = true;
    private Toast toast;

    private String mParam1;
    private String mParam2;

    public static CategoryFragment newInstance(String param1, String param2) {
        CategoryFragment fragment = new CategoryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    protected String getTitle() {
        return null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category, container, false);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(linearLayoutManager);


        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                add(ProductFragment.newInstance("Product Name", "Product ID"));
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));

        ArrayList<CategoryModel> dataList = new ArrayList<>();

        CategoryModel categoryModel1 = new CategoryModel();
        categoryModel1.setId(0);
        categoryModel1.setName("POLAROID");
        categoryModel1.setImageUrl(R.drawable.category1);
        categoryModel1.setPrice("da 12.50€");
        dataList.add(categoryModel1);

        CategoryModel categoryModel2 = new CategoryModel();
        categoryModel2.setId(1);
        categoryModel2.setName("WALL DECORATIONS");
        categoryModel2.setImageUrl(R.drawable.category2);
        categoryModel2.setPrice("da 24.90€");
        dataList.add(categoryModel2);

        CategoryModel categoryModel3 = new CategoryModel();
        categoryModel3.setId(2);
        categoryModel3.setName("PHOTOFRAMES");
        categoryModel3.setImageUrl(R.drawable.category3);
        categoryModel3.setPrice("da 14.90€");
        dataList.add(categoryModel3);

        CategoryModel categoryModel4 = new CategoryModel();
        categoryModel4.setId(3);
        categoryModel4.setName("FRAMES");
        categoryModel4.setImageUrl(R.drawable.category4);
        categoryModel4.setPrice("da 32.50€");
        dataList.add(categoryModel4);

        CategoryAdapter categoryAdapter = new CategoryAdapter(getContext(), dataList);
        recyclerView.setAdapter(categoryAdapter);

        return view;
    }

    @Override
    public boolean onBackPressed() {
        //return true when handled by yourself
        if (consumingBackPress) {
            //This is actually a terrible thing to do and totally against the guidelines
            // Ideally you shouldn't handle the backpress ever, so really think twice about what
            // you are doing and whether you are getting hacky
            toast = Toast.makeText(getActivity(), "Press back once more to quit the application", Toast.LENGTH_LONG);
            toast.show();
            consumingBackPress = false;
            return true; //consumed
        }
        toast.cancel();
        return false; //delegated
    }
}
