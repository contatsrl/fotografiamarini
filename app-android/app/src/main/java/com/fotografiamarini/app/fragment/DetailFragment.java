package com.fotografiamarini.app.fragment;

import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.fotografiamarini.app.R;
import com.fotografiamarini.app.adapter.ImageSliderAdapter;
import com.fotografiamarini.app.base.BaseFragment;
import com.fotografiamarini.app.model.ProductModel;
import com.skydoves.powerspinner.IconSpinnerAdapter;
import com.skydoves.powerspinner.IconSpinnerItem;
import com.skydoves.powerspinner.OnSpinnerItemSelectedListener;
import com.skydoves.powerspinner.PowerSpinnerInterface;
import com.skydoves.powerspinner.PowerSpinnerView;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;
import java.util.List;

public class DetailFragment extends BaseFragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    public static DetailFragment newInstance(String param1, String param2) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    protected String getTitle() {
        return "Product Name";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);

        SliderView sliderView = view.findViewById(R.id.imageSlider);
        ImageSliderAdapter imageSliderAdapter = new ImageSliderAdapter(getContext());
        sliderView.setSliderAdapter(imageSliderAdapter);

        sliderView.setIndicatorAnimation(IndicatorAnimations.NONE); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_RIGHT);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(3);
        sliderView.setAutoCycle(false);

        List<ProductModel> products = new ArrayList<>();
        ProductModel productModel1 = new ProductModel();
        productModel1.setImageUrl(R.drawable.category1);
        products.add(productModel1);

        ProductModel productModel2 = new ProductModel();
        productModel2.setImageUrl(R.drawable.category2);
        products.add(productModel2);

        ProductModel productModel3 = new ProductModel();
        productModel3.setImageUrl(R.drawable.category3);
        products.add(productModel3);

        ProductModel productModel4 = new ProductModel();
        productModel4.setImageUrl(R.drawable.category4);
        products.add(productModel4);

        imageSliderAdapter.renewItems(products);

        Button checkout = (Button) view.findViewById(R.id.checkout);
        checkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add(CheckoutFragment.newInstance());
            }
        });

        ImageButton checkout_float = (ImageButton) view.findViewById(R.id.checkout_float);
        checkout_float.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add(CheckoutFragment.newInstance());
            }
        });

        PowerSpinnerView powerSpinnerView = (PowerSpinnerView) view.findViewById(R.id.powerspinner);
        powerSpinnerView.setOnSpinnerItemSelectedListener(new OnSpinnerItemSelectedListener<String>() {
            @Override
            public void onItemSelected(int position, String item) {
                Toast.makeText(getContext(), item + " selected!", Toast.LENGTH_SHORT).show();
            }
        });

        ArrayList<String> spinnerItems = new ArrayList<>();
        spinnerItems.add("Thickness: 5mm - Height: 40cm - Width: 40cm");
        spinnerItems.add("Thickness: 10mm - Height: 40cm - Width: 40cm");
        spinnerItems.add("Thickness: 15mm - Height: 40cm - Width: 40cm");
        spinnerItems.add("Thickness: 5mm - Height: 20cm - Width: 40cm");
        spinnerItems.add("Thickness: 10mm - Height: 20cm - Width: 40cm");
        spinnerItems.add("Thickness: 15mm - Height: 20cm - Width: 40cm");

        powerSpinnerView.setItems(spinnerItems);
        powerSpinnerView.selectItemByIndex(0);
        powerSpinnerView.setLifecycleOwner(this);

        return view;
    }
}
