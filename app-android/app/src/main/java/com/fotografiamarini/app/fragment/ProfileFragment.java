package com.fotografiamarini.app.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fotografiamarini.app.R;
import com.fotografiamarini.app.base.BaseFragment;

public class ProfileFragment extends BaseFragment {

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected String getTitle() {
        return null;
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        ImageView profileImage = (ImageView) view.findViewById(R.id.profileImage);
        String url = "https://ais-linux6.uvu.edu/idm/blackboard/idphoto.php?imageid=Wm1ZeHR1eEQyT2FWSHpySUxBZ3Bndz09";
        Glide.with(getContext())
                .load(url)
                .apply(RequestOptions.circleCropTransform())
                .into(profileImage);
        return view;
    }
}

