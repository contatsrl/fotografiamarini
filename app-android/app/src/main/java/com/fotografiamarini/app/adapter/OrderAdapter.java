package com.fotografiamarini.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.fotografiamarini.app.R;
import com.fotografiamarini.app.fragment.CheckoutFragment;
import com.fotografiamarini.app.model.OrderModel;

import java.util.ArrayList;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {

    private ArrayList<OrderModel> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private CheckoutFragment checkoutFragment;

    public OrderAdapter(CheckoutFragment context, ArrayList<OrderModel> data) {
        this.checkoutFragment = context;
        this.mInflater = LayoutInflater.from(context.getContext());
        this.mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.order_adapter_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        OrderModel order = getItem(position);

        holder.orderDetails.setText(order.getDescription());
        holder.orderCount.setText(String.valueOf(order.getOrder_count()));
        holder.orderPrice.setText(String.valueOf(order.getPrice()));
        holder.imageUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkoutFragment.openImageUploader();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView orderDetails, orderCount, orderPrice;
        Button imageUpload;

        ViewHolder(View itemView) {
            super(itemView);
            orderDetails = itemView.findViewById(R.id.orderDetails);
            orderCount = itemView.findViewById(R.id.orderCount);
            orderPrice = itemView.findViewById(R.id.orderPrice);
            imageUpload = itemView.findViewById(R.id.imageUpload);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    OrderModel getItem(int id) {
        return mData.get(id);
    }

    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}