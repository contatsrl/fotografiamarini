package com.fotografiamarini.app.model;

public class ProductModel {
    private int id =0;
    private String name = "";
    private int imageUrl = 0;
    private String price = "0.0€";
    private String description = "";
    private String information = "";
    private DementionModel[] demention;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(int imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String descriptioin) {
        this.description = descriptioin;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public DementionModel[] getDemention() {
        return demention;
    }

    public void setDemention(DementionModel[] demention) {
        this.demention = demention;
    }
}