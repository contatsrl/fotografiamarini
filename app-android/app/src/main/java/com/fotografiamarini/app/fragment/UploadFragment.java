package com.fotografiamarini.app.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fotografiamarini.app.R;
import com.fotografiamarini.app.base.BaseFragment;

import antonkozyriatskyi.circularprogressindicator.CircularProgressIndicator;

public class UploadFragment extends BaseFragment {

    public static UploadFragment newInstance() {
        return new UploadFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected String getTitle() {
        return "Uploading";
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_upload, container, false);

        CircularProgressIndicator circularProgressIndicator = (CircularProgressIndicator) view.findViewById(R.id.circular_progress);
        circularProgressIndicator.setMaxProgress(100);
        circularProgressIndicator.setCurrentProgress(37);
        circularProgressIndicator.setProgressTextAdapter(PERCENT_ADAPTER);
        return view;
    }

    private static final CircularProgressIndicator.ProgressTextAdapter PERCENT_ADAPTER = new CircularProgressIndicator.ProgressTextAdapter() {
        @Override
        public String formatText(double time) {
            String percent = ((int) time) + "%";
            return percent;
        }
    };
}

