package com.fotografiamarini.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.fotografiamarini.app.R;
import com.fotografiamarini.app.model.ImageUploadModel;
import com.fotografiamarini.app.model.OrderModel;

import java.util.ArrayList;

public class ImageUploaderAdapter extends RecyclerView.Adapter<ImageUploaderAdapter.ViewHolder> {

    private ArrayList<ImageUploadModel> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    private Context mContext;

    public ImageUploaderAdapter(Context context, ArrayList<ImageUploadModel> data) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.image_uploader_adapter_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        ImageUploadModel data = getItem(position);

        holder.selectImage.setText(String.valueOf(data.getId()));
        if (data.getUploaded()) {
            holder.selectImage.setBackgroundResource(R.drawable.corner_radius_green_background);
            holder.selectImage.setTextColor(mContext.getResources().getColor(R.color.colorWhite));
        } else {
            holder.selectImage.setBackgroundResource(R.drawable.corner_radius_light_gray_background);
            holder.selectImage.setTextColor(mContext.getResources().getColor(R.color.colorGray));
        }

        holder.selectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.selectImage.setBackgroundResource(R.drawable.corner_radius_primary_background);
                holder.selectImage.setTextColor(mContext.getResources().getColor(R.color.colorWhite));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        Button selectImage;

        ViewHolder(View itemView) {
            super(itemView);
            selectImage = itemView.findViewById(R.id.selectImage);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    ImageUploadModel getItem(int id) {
        return mData.get(id);
    }

    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
