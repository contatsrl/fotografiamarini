package com.fotografiamarini.app.activity;


import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.fotografiamarini.app.App;
import com.fotografiamarini.app.R;
import com.fotografiamarini.app.base.BaseActivity;
import com.fotografiamarini.app.fragment.CategoryFragment;
import com.fotografiamarini.app.fragment.ProfileFragment;

import java.util.Objects;

public class MainActivity extends BaseActivity implements ListView.OnItemClickListener {

    ListView drawerList;
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    ImageView logo;

    private ActionBarDrawerToggle drawerToggle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawerList = (ListView) findViewById(R.id.main_navigation);
        drawerList.setOnItemClickListener(this);
        drawerLayout = (DrawerLayout) findViewById(R.id.main_drawer);
        toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        logo = (ImageView) findViewById(R.id.logo);
        App.setLogo(logo);

        setupNavigationItems();
        setupDrawerAndToggle();
        gotoCategory();
    }

    private void setupDrawerAndToggle() {
        setSupportActionBar(toolbar);
//        Objects.requireNonNull(getSupportActionBar()).setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//        getSupportActionBar().setCustomView(R.layout.custom_toolbar);
//        TextView textView = getSupportActionBar().getCustomView().findViewById(R.id.toolbarTitle);
//        textView.setText("My Custom Title");
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            getSupportActionBar().setIcon(R.drawable.logo);
//            getSupportActionBar().setLogo(R.drawable.logo_toolbar);
        }
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, 0, 0) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                setDrawerIndicatorEnabled(true);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }

    private void setupNavigationItems() {
        String[] navigationItems = {"Profile", "Products", "Contacts"};
        ArrayAdapter<String> mAdapter = new ArrayAdapter<>(this, R.layout.menu_item, navigationItems);
        drawerList.setAdapter(mAdapter);
    }

    private void gotoProfile() {
        logo.setVisibility(View.INVISIBLE);
        add(ProfileFragment.newInstance());
    }

    private void gotoCategory() {
        logo.setVisibility(View.VISIBLE);
        add(CategoryFragment.newInstance("Category", "Content"));
    }

    private void gotoContacts() {
    }

    @Override
    protected DrawerLayout getDrawer() {
        return drawerLayout;
    }

    @Override
    protected ActionBarDrawerToggle getDrawerToggle() {
        return drawerToggle;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                gotoProfile();
                break;
            case 1:
                gotoCategory();
                break;
            case 2:
                gotoContacts();
                break;
            default:
                break;
        }
        drawerLayout.closeDrawer(drawerList);
    }
}
