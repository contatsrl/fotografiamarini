package com.fotografiamarini.app.fragment;

import android.graphics.Matrix;
import android.graphics.PointF;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fotografiamarini.app.R;
import com.fotografiamarini.app.listener.RecyclerItemClickListener;
import com.fotografiamarini.app.adapter.ImageUploaderAdapter;
import com.fotografiamarini.app.base.BaseFragment;
import com.fotografiamarini.app.model.ImageUploadModel;

import java.util.ArrayList;

public class ImageUploaderFragment extends BaseFragment implements View.OnTouchListener {

    private static final float MIN_ZOOM = 1f,MAX_ZOOM = 1f;

    // These matrices will be used to scale points of the image
    Matrix matrix = new Matrix();
    Matrix savedMatrix = new Matrix();

    // The 3 states (events) which the user is trying to perform
    static final int NONE = 0;
    static final int DRAG = 1;
    static final int ZOOM = 2;
    int mode = NONE;

    // these PointF objects are used to record the point(s) the user is touching
    PointF start = new PointF();
    PointF mid = new PointF();
    float oldDist = 1f;



    public static ImageUploaderFragment newInstance() {
        ImageUploaderFragment fragment = new ImageUploaderFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    protected String getTitle() {
        return "Images uploader";
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_image_uploader, container, false);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));

        ArrayList<ImageUploadModel> dataList = new ArrayList<>();
        ImageUploadModel imageUploadModel1 = new ImageUploadModel();
        imageUploadModel1.setId(1);
        imageUploadModel1.setUploaded(true);
        dataList.add(imageUploadModel1);

        ImageUploadModel imageUploadModel2 = new ImageUploadModel();
        imageUploadModel2.setId(2);
        imageUploadModel2.setUploaded(true);
        dataList.add(imageUploadModel2);

        ImageUploadModel imageUploadModel3 = new ImageUploadModel();
        imageUploadModel3.setId(3);
        imageUploadModel3.setUploaded(true);
        dataList.add(imageUploadModel3);

        ImageUploadModel imageUploadModel4 = new ImageUploadModel();
        imageUploadModel4.setId(4);
        imageUploadModel4.setUploaded(false);
        dataList.add(imageUploadModel4);

        ImageUploadModel imageUploadModel5 = new ImageUploadModel();
        imageUploadModel5.setId(5);
        imageUploadModel5.setUploaded(false);
        dataList.add(imageUploadModel5);

        ImageUploadModel imageUploadModel6 = new ImageUploadModel();
        imageUploadModel6.setId(6);
        imageUploadModel6.setUploaded(false);
        dataList.add(imageUploadModel6);

        ImageUploadModel imageUploadModel7 = new ImageUploadModel();
        imageUploadModel7.setId(7);
        imageUploadModel7.setUploaded(false);
        dataList.add(imageUploadModel7);

        ImageUploadModel imageUploadModel8 = new ImageUploadModel();
        imageUploadModel8.setId(8);
        imageUploadModel8.setUploaded(false);
        dataList.add(imageUploadModel8);

        ImageUploadModel imageUploadModel9 = new ImageUploadModel();
        imageUploadModel9.setId(9);
        imageUploadModel9.setUploaded(false);
        dataList.add(imageUploadModel9);

        ImageUploadModel imageUploadModel10 = new ImageUploadModel();
        imageUploadModel10.setId(10);
        imageUploadModel10.setUploaded(false);
        dataList.add(imageUploadModel10);

        ImageUploaderAdapter categoryAdapter = new ImageUploaderAdapter(getContext(), dataList);
        recyclerView.setAdapter(categoryAdapter);

        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
        imageView.setOnTouchListener(this);

        Button uploadImage = (Button) view.findViewById(R.id.uploadImage);
        uploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("ImageUpload", "Clicked");
                add(UploadFragment.newInstance());
            }
        });

        return view;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event)
    {
        ImageView view = (ImageView) v;
        view.setScaleType(ImageView.ScaleType.MATRIX);
        float scale;

        dumpEvent(event);
        // Handle touch events here...

        switch (event.getAction() & MotionEvent.ACTION_MASK)
        {
            case MotionEvent.ACTION_DOWN:   // first finger down only
                savedMatrix.set(matrix);
                start.set(event.getX(), event.getY());
                mode = DRAG;
                break;

            case MotionEvent.ACTION_UP: // first finger lifted

            case MotionEvent.ACTION_POINTER_UP: // second finger lifted

                mode = NONE;
                break;

            case MotionEvent.ACTION_POINTER_DOWN: // first and second finger down

                oldDist = spacing(event);
                if (oldDist > 5f) {
                    savedMatrix.set(matrix);
                    midPoint(mid, event);
                    mode = ZOOM;
                }
                break;

            case MotionEvent.ACTION_MOVE:

                if (mode == DRAG)
                {
                    matrix.set(savedMatrix);
                    matrix.postTranslate(event.getX() - start.x, event.getY() - start.y); // create the transformation in the matrix  of points
                }
                else if (mode == ZOOM)
                {
                    // pinch zooming
                    float newDist = spacing(event);
                    if (newDist > 5f)
                    {
                        matrix.set(savedMatrix);
                        scale = newDist / oldDist; // setting the scaling of the
                        // matrix...if scale > 1 means
                        // zoom in...if scale < 1 means
                        // zoom out
                        matrix.postScale(scale, scale, mid.x, mid.y);
                    }
                }
                break;
        }

        view.setImageMatrix(matrix); // display the transformation on screen

        return true; // indicate event was handled
    }

    /*
     * --------------------------------------------------------------------------
     * Method: spacing Parameters: MotionEvent Returns: float Description:
     * checks the spacing between the two fingers on touch
     * ----------------------------------------------------
     */

    private float spacing(MotionEvent event)
    {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);
    }

    /*
     * --------------------------------------------------------------------------
     * Method: midPoint Parameters: PointF object, MotionEvent Returns: void
     * Description: calculates the midpoint between the two fingers
     * ------------------------------------------------------------
     */

    private void midPoint(PointF point, MotionEvent event)
    {
        float x = event.getX(0) + event.getX(1);
        float y = event.getY(0) + event.getY(1);
        point.set(x / 2, y / 2);
    }

    /** Show an event in the LogCat view, for debugging */
    private void dumpEvent(MotionEvent event)
    {
        String names[] = { "DOWN", "UP", "MOVE", "CANCEL", "OUTSIDE","POINTER_DOWN", "POINTER_UP", "7?", "8?", "9?" };
        StringBuilder sb = new StringBuilder();
        int action = event.getAction();
        int actionCode = action & MotionEvent.ACTION_MASK;
        sb.append("event ACTION_").append(names[actionCode]);

        if (actionCode == MotionEvent.ACTION_POINTER_DOWN || actionCode == MotionEvent.ACTION_POINTER_UP)
        {
            sb.append("(pid ").append(action >> MotionEvent.ACTION_POINTER_ID_SHIFT);
            sb.append(")");
        }

        sb.append("[");
        for (int i = 0; i < event.getPointerCount(); i++)
        {
            sb.append("#").append(i);
            sb.append("(pid ").append(event.getPointerId(i));
            sb.append(")=").append((int) event.getX(i));
            sb.append(",").append((int) event.getY(i));
            if (i + 1 < event.getPointerCount())
                sb.append(";");
        }

        sb.append("]");
        Log.d("Touch Events ---------", sb.toString());
    }
}
